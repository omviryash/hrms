<?php include_once 'menu.php'; ?>
<?php
$emp_id = isset($_POST['employee_id'])?$_POST['employee_id']:11;
?>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Employee Leave Information</h3>
                    </div>
                    <form role="form" method="post" action="leave_report.php" id="frm-leave-data">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Employee Name:</label>
                                    <select  class="form-control" name="employee_id" id="employee-select">
                                        <?php
                                        $sql = "Select * from employee";
                                        $data = $db->fetch($sql);
                                        for($i=0;$i<count($data);$i++) {
                                            ?>
                                            <option value="<?= $data[$i]['id']; ?>" <?= $data[$i]['id'] == $emp_id?'selected="selected"':'';?>><?= $data[$i]['empname']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            // $emp_id = isset($_POST['emp_id'])?$_POST['emp_id']:11;
                            $employee_data = getEmpById($emp_id);
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2">Employee name</div>
                                <div class="col-md-8"><?=$employee_data['empname']?></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">Date of joining</div>
                                <div class="col-md-8"><?=$employee_data['dateofjoinin']?></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">Prob. Period</div>
                                <div class="col-md-8"><?=$employee_data['prodate']?></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">Conformation date</div>
                                <div class="col-md-8"><?=$employee_data['confdate']?></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leave Report</h3>

                        <div class="leave-report-wrapper">
                            <div class="table-responsive">
                                <?php
                                    $query = "SELECT YEAR(leave_date) as year, MONTH(leave_date) as month, SUM(CASE WHEN `leave_type` NOT IN('P','WH') THEN 1 ELSE 0 END) as taken_leave FROM `emp_leave` WHERE employee_id = '$emp_id' GROUP BY YEAR(leave_date),MONTH(leave_date)";
                                    $leaves = $db->fetch($query);
                                    $db = new db_connect();
                                    $leave_reports_data = $db->get_employee_leave_report($emp_id);
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Month</th>
                                            <th>Taken</th>
                                            <th>Paid</th>
                                            <th>Without paid</th>
                                            <th>Leave balance</th>
                                            <th>Present Days</th>
                                            <th>Salary</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(count($leaves) > 0){
                                            foreach($leaves as $leave){
                                                $total_days = $db->get_month_days($leave['month'],$leave['year']);
                                                $paid_leave = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['paid_leave'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['paid_leave']:0;
                                                $unpaid_leave = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['unpaid_leave'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['unpaid_leave']:0;
                                                $leave_balance = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['leave_balance'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['leave_balance']:0;
                                                $present_days = $total_days - $unpaid_leave;
                                                if($unpaid_leave > 0){
                                                    $salary = $employee_data['salary'];
                                                    //$salary_per_day = $salary / $db->get_month_days(str_pad($leave['month'],2,'0',STR_PAD_LEFT),$leave['year']);
                                                    $salary_per_day = $salary / 30;
                                                    $salary = $salary - ($unpaid_leave * $salary_per_day);
                                                    $salary = round($salary,2);
                                                }else{
                                                    $salary = $employee_data['salary'];
                                                }
                                                ?>
                                                <tr>
                                                    <td><?=$leave['year'].'-'.str_pad($leave['month'],2,'0',STR_PAD_LEFT);?></td>
                                                    <td><?=$leave['taken_leave'];?></td>
                                                    <td><?=$paid_leave;?></td>
                                                    <td><?=$unpaid_leave;?></td>
                                                    <td><?=$leave_balance;?></td>
                                                    <td><?=$present_days;?></td>
                                                    <td><?=$salary;?></td>
                                                </tr>
                                    <?php
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Employee Leave Information</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-border">
                            <thead>
                            <tr>
                                <th>Month</th>
                                <th>Leave</th>
                                <?php
                                for($i = 1;$i < 32;$i++) {
                                    ?>
                                    <th><?=$i?></th>
                                    <?php
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $month_query = "SELECT YEAR(leave_date) as year,DATE_FORMAT(leave_date,'%m') as month, SUM(CASE WHEN `leave_type` NOT IN('P','WH') THEN 1 ELSE 0 END) as taken_leave FROM `emp_leave` WHERE employee_id = '$emp_id' GROUP BY CONCAT(YEAR(leave_date),DATE_FORMAT(leave_date,'%m')) ORDER BY CONCAT(YEAR(leave_date),DATE_FORMAT(leave_date,'%m'))";
                            $month_result = $db->query($month_query);
                            if(mysqli_num_rows($month_result) > 0) {
                                while($month_row = mysqli_fetch_object($month_result)){
                                    ?>
                                    <tr>
                                        <td><?=$month_row->year.'-'.$month_row->month?></td>
                                        <td><?=$month_row->taken_leave?></td>
                                        <?php
                                        $year_month = $month_row->year.$month_row->month;
                                        $day_query = "SELECT * FROM `emp_leave` WHERE employee_id = '$emp_id' AND CONCAT(YEAR(leave_date),DATE_FORMAT(leave_date,'%m')) = '$year_month' ORDER BY leave_date ASC";
                                        $day_result = $db->query($day_query);
                                        $day_counter = 1;
                                        while($day_row = mysqli_fetch_object($day_result)){
                                            echo "<td>".$day_row->leave_type."</td>";
                                            $day_counter++;
                                        }
                                        while($day_counter < 32){
                                            echo "<td>-</td>";
                                            $day_counter++;
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div>
    <strong>Copyright &copy; 2016-2017<a href="#"> &nbsp;OM</a>.</strong> All rights reserved.
</footer>
<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="plugins/morris/morris.min.js"></script> -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('#leave_date').daterangepicker({
            "locale": {
                "format": "DD/MM/YYYY"
            }
        });
        $('#leave-report-table').DataTable();
        $(document).on('change','#employee-select',function(){
            $("#frm-leave-data").submit();
        });
    });
</script>
</body>
</html>
