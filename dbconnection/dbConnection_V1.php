<?php
class db_connect {
    var $db;
    function db_connect() {
	    if($_SERVER["SERVER_NAME"]=='127.0.0.1' || $_SERVER["SERVER_NAME"] == 'localhost'){
	     	$this->db = mysqli_connect ('localhost', 'root', '','hrms') or die ("unable to connect to database server");
	     }else{
	     	$this->db = mysqli_connect ('localhost', 'root', '','hrms') or die ("unable to connect to database server");
	     //	$this->db = mysqli_connect ('localhost', 'hacinfot_sehyon', 'ZZ^Iks#1H4oa','hacinfot_sehyon') or die ("unable to connect to database server");
   // 	 	$this->db = mysqli_connect ('localhost', 'root', '','sehyon') or die ("unable to connect to database server");
	     }
	}
	
    
    function query($sql) {
          $result = mysqli_query($this->db,$sql) or die ("invalid query: " . mysql_error());
          return $result;
    }
    function fetch($sql) {
        $data = array();
        $result = $this->query($sql);
        while ($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    }

    function fetch_array($sql) {
        return mysqli_fetch_assoc($this->query($sql));
    }
    
    function getone($sql) {
        $result = $this->query($sql);
        if (mysql_num_rows($result) == 0)
            $value = false;
        else
            $value = mysql_result($result, 0);
        return $value;
    }
    
    function getOneRow($sql){ 
        $data = array();
        $result = $this->query($sql);
        $row = $result->fetch_assoc();
        return $row;
    }

    function save($sql) {
    	if(!empty($sql)){
    		mysqli_query($this->db,$sql) or die("invalid query: " . mysql_error());
    		return mysqli_insert_id($this->db);
    	}
    }
    
    
    function getCount($sql){
        $data;
        $result = $this->query($sql);
        $row = mysql_fetch_array($result);
        $data =   $row[0];
        return $data;
    }
    
    function yyyymmdd($dateOld) {
    	$explode=explode("/",$dateOld);
//    	print_r($explode);
//    	$explode[0]=substr($explode[0], -1, 2);
    	$new_date=$explode[2]."-".$explode[1]."-".$explode[0];
        return $new_date;
    }

    function ddmmyyyy($dateOld) {
    	if(!empty($dateOld)){
    		return date("d/m/Y", strtotime($dateOld));
    	} else {
    		return ;
    	}
    }
    
    function verifyEmail($email){
    	$email = $this->fetch("Select count(id) AS cnt from users where username='".$email."'");
    	$result;
    	if(intval($email[0]['cnt'])>0){
    		return true;
    	}else{
    		return false;
    	}
    }
    function close(){
    	if(isset($this->db)){
    	 mysql_close($this->db);
    	}
    }
    
    /***
     * 
     * for display limited string
     */
    function limit_words($string, $word_limit){
    	$words = explode(" ",$string);
    	return implode(" ", array_splice($words, 0, $word_limit));
    }
    
	function create_slug($string, $ext='.html'){
	        $string = strtolower($string);//convert the string to lowercase
	     $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);//strip non alpha-numeric characters
	     $string = preg_replace("/[\s-]+/", " ", $string);//remove multiple dashes or whitespaces
	     $string = preg_replace("/[\s_]/", "-", $string);//transform whitespaces and underscore to dash
	
	    return $string.$ext;
	}
	
	function select($sql){
		$data = $this->fetch($sql);
		$select = htmlentities('<option value="0">no record found</option>');
		if(count($data)>0){
			for($i=0;$i<count($data);$i++){
				$select.= htmlentities("<option value='".$data[$i]['id']."'>".$data[$i]['name']."</option>");
			}
		}
		return $select;
	}
	
	function selectWithId($sql,$selectedId){
		$data = $this->fetch($sql);
		$select = htmlentities("<option value='0'>no record found</option>");
		if(count($data)>0){
			for($i=0;$i<count($data);$i++){
				$selelected ="";
				if($selectedId == $data[$i]['id']){
					$selelected = "selected='selected'";
				}
				$select.= htmlentities("<option ".$selelected." value='".$data[$i]['id']."'>".$data[$i]['name']."</option>");
			}
		}
		return $select ;
	}
	function selectBox($tableName,$selectedId,$selectBoxName){
		$selectBox = "";
		$selectBox = "<select  class='form-control' id=".$selectBoxName." name=".$selectBoxName.">";			
		$selectBox .= "<option value='-1'>Select ".$tableName."</option>";
		$data = $this->fetch("Select * from ".$tableName);
		for($i=0;$i<count($data);$i++){
			if($selectedId==$data[$i]['id']){
				$selectBox .= "<option  selected='selected' value=".$data[$i]['id'].">".$data[$i]['name']."</option>";
			}else{
				$selectBox .= "<option value=".$data[$i]['id'].">".$data[$i]['name']."</option>";
			}
		}
		
		return  $selectBox."</select>";
	}
	
	function selectBoxByQuery($name,$query,$selectedId,$selectBoxName){
		$selectBox = "";
		$selectBox = "<select  class='form-control' id=".$selectBoxName." name=".$selectBoxName.">";
		$selectBox .= "<option value='-1'>Select ".$name."</option>";
		$data = $this->fetch($query);
		for($i=0;$i<count($data);$i++){
			if($selectedId==$data[$i]['id']){
				$selectBox .= "<option  selected='selected' value=".$data[$i]['id'].">".$data[$i]['name']."</option>";
			}else{
				$selectBox .= "<option value=".$data[$i]['id'].">".$data[$i]['name']."</option>";
			}
		}
		return  $selectBox."</select>";
	}

	/**
	 * @param $employee_id
	 * @param $month
	 * @return mixed
	 */
	function count_salary($employee_id,$month){
		$query = "SELECT salary FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$result = mysqli_query($this->db,$query);
		$row = mysqli_fetch_object($result);
		$per_day_salary = $row->salary / $this->curr_month_days();
		$monthly_salary = $row->salary;
		$total_leave = $this->get_month_total_leave($employee_id,$month);
		/*echo $total_leave;die;*/
		$leave_reduce = $per_day_salary * $total_leave;
		$monthly_salary = $monthly_salary -$leave_reduce;
		return round($monthly_salary,2);
	}

	function get_month_total_leave($employee_id,$month){
		$query = "SELECT * FROM `emp_leave` WHERE employee_id = '$employee_id' AND MONTH(timestamp) = '$month' ORDER BY id ";
		$result = mysqli_query($this->db,$query);
		$total_leave = 0;
		while($row = mysqli_fetch_object($result)){
			if($row->leave_type != 'P' && $row->leave_type != 'WH'){
				$total_leave++;
			}
		}
		return $total_leave;
	}

	function get_employee_leave_report($employee_id){
		$leave_report = array();
		$current_date = date('Y-m-d');

		/*---------- Employee Data ---------------*/
		$employee_query = "SELECT * FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$employee_result = mysqli_query($this->db, $employee_query);
		$employee_row = mysqli_fetch_object($employee_result);
		$employee_allotment_date = $this->get_emp_allotment_from_date($employee_id);
		$employee_avail_from_date = $this->get_emp_avail_from_date($employee_id);

		/*-------------- Leave Policy ------------------*/

		$leave_policy_query = "SELECT * FROM `company_sick_leave_policy` WHERE 1 LIMIT 1";
		$leave_policy_result = mysqli_query($this->db, $leave_policy_query);
		$leave_policy_row = mysqli_fetch_object($leave_policy_result);
		$allot_from = $leave_policy_row->allot_from;
		$allot_after_month = $leave_policy_row->allot_after_month;
		$allot_after_days = $leave_policy_row->allot_after_days;
		$total_leaves = $leave_policy_row->total_leaves;
		$allotment_at = $leave_policy_row->allot;  // Monthly || Quarterly || Half-Year || Yearly


		/*---------------------------------------*/
		$months_last_date = array('');
		$total_days = floor((strtotime(date('Y-m-d')) - strtotime($employee_allotment_date))/3600/24);
		$allotment_interval_add_leave = 0;
		if ($total_days > 0){
			if (strtolower($allotment_at) == 'monthly'){
				$allotment_interval_add_leave = (int)$total_leaves / 12;
				$allotment_leaves = 0;
				$data = array();
				for($i=strtotime($employee_allotment_date);$i<= strtotime(date('Y-m-d'));$i = strtotime("+1 months",$i)){
					$allotment_leaves = $allotment_leaves + $allotment_interval_add_leave;
					$data[date('Ym',$i)] = $allotment_leaves;
					$months_last_date[] = $this->month_last_date(date('m',$i),date('Y',$i));

				}

			}else if (strtolower($allotment_at) == 'quarterly'){
				$allotment_interval_add_leave = (int)$total_leaves / 4;
			} else if (strtolower($allotment_at) == 'half_yearly'){
				$allotment_interval_add_leave = (int)$total_leaves / 2;
			}else if (strtolower($allotment_at) == 'yearly'){
				$allotment_interval_add_leave = (int)$total_leaves;
			}
		}else{
			$allotment = 0;
		}
		/*echo "<pre/>";
		print_r($months_last_date);die;*/

		/*---------- Leave Data ---------------*/
		$leave_query = "SELECT * FROM `emp_leave` WHERE employee_id = '$employee_id' AND leave_date <= '2016-12-31' ORDER BY id ";
		$leave_result = mysqli_query($this->db,$leave_query);
		$leave_balance = 0;
		$unpaid_leave = 0;
		$paid_leave = 0;
		$taken_sl_leave = 0;
		while($leave_row = mysqli_fetch_object($leave_result)) {
			/*----------- if month complete ------------*/
			if(in_array($leave_row->leave_date,$months_last_date)){
				$leave_balance = $leave_balance + $allotment_interval_add_leave;
			}
			if ($leave_row->leave_type == 'SL') {
				$taken_sl_leave++;
				if($employee_avail_from_date > $leave_row->leave_date){
					$unpaid_leave++;
				}elseif($employee_avail_from_date <= $leave_row->leave_date){
					if($leave_balance > 0){
						$leave_balance--;
						$paid_leave++;
					}else{
						$unpaid_leave++;
					}
				}
			}
		}
		echo $paid_leave;die;

		while($leave_row = mysqli_fetch_object($leave_result)){

			if($leave_row->leave_type == 'SL'){
				/*--------Total---------*/
				/*$leave_report['total_taken_sl_leave'] = isset($leave_report['total_taken_sl_leave'])?$leave_report['total_taken_sl_leave']:0;
				$leave_report['total_taken_sl_leave'] = $leave_report['total_taken_sl_leave'] + 1;*/

				/*-------- Month Wise ---------*/
				$leave_report[date('Ym',strtotime($leave_row->leave_date))]['taken_sl_leave'] = isset($leave_report[date('Ym',strtotime($leave_row->leave_date))]['taken_sl_leave'])?$leave_report[date('Ym',strtotime($leave_row->leave_date))]['taken_sl_leave']:0;
				$leave_report[date('Ym',strtotime($leave_row->leave_date))]['taken_sl_leave'] = $leave_report[date('Ym',strtotime($leave_row->leave_date))]['taken_sl_leave'] + 1;
				if($employee_avail_from_date > $leave_row->leave_date){
					$leave_report[date('Ym',strtotime($leave_row->leave_date))]['unpaid_leave'] = isset($leave_report[date('Ym',strtotime($leave_row->leave_date))]['unpaid_leave'])?$leave_report[date('Ym',strtotime($leave_row->leave_date))]['unpaid_leave']:0;
					$leave_report[date('Ym',strtotime($leave_row->leave_date))]['unpaid_leave'] = $leave_report[date('Ym',strtotime($leave_row->leave_date))]['unpaid_leave'] + 1;

					$leave_report['total_unpaid_leave'] = isset($leave_report['total_unpaid_leave'])?$leave_report['total_unpaid_leave']:0;
					$leave_report['total_unpaid_leave'] = $leave_report['total_unpaid_leave'] + 1;
				}elseif($employee_avail_from_date <= $leave_row->leave_date){
					$leave_report[date('Ym',strtotime($leave_row->leave_date))]['leave_add_counter'] = isset($data[date('Ym',strtotime($leave_row->leave_date))])?$data[date('Ym',strtotime($leave_row->leave_date))]:0;
					$leave_report[date('Ym',strtotime($leave_row->leave_date))]['leave_balance'] = isset($leave_report[date('Ym',strtotime($leave_row->leave_date))]['leave_balance'])?$leave_report[date('Ym',strtotime($leave_row->leave_date))]['leave_balance']:0;
				}
			}
			$leave_report[date('Ym',strtotime($leave_row->leave_date))]['taken_sl_leave'] = isset($leave_report[date('Ym',strtotime($leave_row->leave_date))]['taken_sl_leave'])?$leave_report[date('Ym',strtotime($leave_row->leave_date))]['taken_sl_leave']:0;
			$leave_report[date('Ym',strtotime($leave_row->leave_date))]['paid_leave'] = isset($leave_report[date('Ym',strtotime($leave_row->leave_date))]['paid_leave'])?$leave_report[date('Ym',strtotime($leave_row->leave_date))]['paid_leave']:0;
			$leave_report[date('Ym',strtotime($leave_row->leave_date))]['unpaid_leave'] = isset($leave_report[date('Ym',strtotime($leave_row->leave_date))]['unpaid_leave'])?$leave_report[date('Ym',strtotime($leave_row->leave_date))]['unpaid_leave']:0;
			$leave_report[date('Ym',strtotime($leave_row->leave_date))]['leave_balance'] = isset($leave_report[date('Ym',strtotime($leave_row->leave_date))]['leave_balance'])?$leave_report[date('Ym',strtotime($leave_row->leave_date))]['leave_balance']:0;
			$leave_report[date('Ym',strtotime($leave_row->leave_date))]['leave_add_counter'] = isset($data[date('Ym',strtotime($leave_row->leave_date))])?$data[date('Ym',strtotime($leave_row->leave_date))]:0;
		}
		echo "<pre>";
		print_r($leave_report);
		die;
		return $leave_report;
	}

	function month_last_date($month = '', $year = '') {
		if (empty($month)) {
			$month = date('m');
		}
		if (empty($year)) {
			$year = date('Y');
		}
		$result = strtotime("{$year}-{$month}-01");
		$result = strtotime('-1 second', strtotime('+1 month', $result));
		return date('Y-m-d', $result);
	}

	function get_emp_allotment_from_date($employee_id)
	{
		$allotment_date = 0;

		/*------------- Fetch Employee Data ------------------------*/
		$employee_query = "SELECT * FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$employee_result = mysqli_query($this->db, $employee_query);
		$employee_row = mysqli_fetch_object($employee_result);
		$date_of_joining = $employee_row->dateofjoinin;
		$confirmation_date = $employee_row->confdate;
		$prob_date = $employee_row->prodate;

		/*---------- Fetch Sick Leave Policy ----------------------*/
		$leave_policy_query = "SELECT * FROM `company_sick_leave_policy` WHERE 1 LIMIT 1";
		$leave_policy_result = mysqli_query($this->db, $leave_policy_query);
		$leave_policy_row = mysqli_fetch_object($leave_policy_result);
		$allot_from = $leave_policy_row->allot_from;
		$allot_after_month = $leave_policy_row->allot_after_month;
		$allot_after_days = $leave_policy_row->allot_after_days;

		if (strtolower($allot_from) == 'doj') { // Date of joining
			$allotment_date = $date_of_joining;
		} elseif (strtolower($allot_from) == 'pd') { // Prob. period
			$allotment_date = $prob_date;
		} elseif (strtolower($allot_from) == 'cd') { // Conformation date
			$allotment_date = $confirmation_date;
		}

		$allotment_date_string = strtotime($allotment_date);
		if ($allot_after_month && $allot_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $allot_after_month . " month", $allotment_date_string)));
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $allot_after_days . " day", $allotment_date_string)));
		} else if ($allot_after_month && !$allot_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $allot_after_month . " month", $allotment_date_string)));
		} else if (!$allot_after_month && $allot_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $allot_after_days . " day", $allotment_date_string)));
		}
		$allotment_date = date("Y-m-d", $allotment_date_string);
		return $allotment_date;
	}

	function get_emp_avail_from_date($employee_id)
	{
		$avail_from_date = 0;

		/*------------- Fetch Employee Data ------------------------*/
		$employee_query = "SELECT * FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$employee_result = mysqli_query($this->db, $employee_query);
		$employee_row = mysqli_fetch_object($employee_result);
		$date_of_joining = $employee_row->dateofjoinin;
		$confirmation_date = $employee_row->confdate;
		$prob_date = $employee_row->prodate;

		/*---------- Fetch Sick Leave Policy ----------------------*/
		$leave_policy_query = "SELECT * FROM `company_sick_leave_policy` WHERE 1 LIMIT 1";
		$leave_policy_result = mysqli_query($this->db, $leave_policy_query);
		$leave_policy_row = mysqli_fetch_object($leave_policy_result);
		$avail_from = $leave_policy_row->avail_from;
		$avail_after_month = $leave_policy_row->avail_after_month;
		$avail_after_days = $leave_policy_row->avail_after_days;

		if (strtolower($avail_from) == 'doj') { // Date of joining
			$avail_from_date = $date_of_joining;
		} elseif (strtolower($avail_from) == 'pd') { // Prob. period
			$avail_from_date = $prob_date;
		} elseif (strtolower($avail_from) == 'cd') { // Conformation date
			$avail_from_date = $confirmation_date;
		}

		$allotment_date_string = strtotime($avail_from_date);
		if ($avail_after_month && $avail_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $avail_after_month . " month", $allotment_date_string)));
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $avail_after_days . " day", $allotment_date_string)));
		} else if ($avail_after_month && !$avail_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $avail_after_month . " month", $allotment_date_string)));
		} else if (!$avail_after_month && $avail_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $avail_after_days . " day", $allotment_date_string)));
		}
		$avail_from_date = date("Y-m-d", $allotment_date_string);
		return $avail_from_date;
	}

	/**
	 * @return int
	 */
	function curr_month_days(){
		$month = date('m');
		$year = date('Y');
		return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
	}
}
?>