<?php
class db_connect {
    var $db;
    function db_connect() {
	    if($_SERVER["SERVER_NAME"]=='127.0.0.1' || $_SERVER["SERVER_NAME"] == 'localhost'){
	     	$this->db = mysqli_connect ('localhost', 'root', '','hrms') or die ("unable to connect to database server");
	     }else{
			$this->db = mysqli_connect ('localhost', 'omvir_hrms','hrms1234','omvir_hrms') or die ("unable to connect to database server");
	     	//$this->db = mysqli_connect ('localhost', 'root', '','hrms') or die ("unable to connect to database server");
	     //	$this->db = mysqli_connect ('localhost', 'hacinfot_sehyon', 'ZZ^Iks#1H4oa','hacinfot_sehyon') or die ("unable to connect to database server");
   // 	 	$this->db = mysqli_connect ('localhost', 'root', '','sehyon') or die ("unable to connect to database server");
	     }
	}
	
    
    function query($sql) {
          $result = mysqli_query($this->db,$sql) or die ("invalid query: " . mysql_error());
          return $result;
    }
    function fetch($sql) {
        $data = array();
        $result = $this->query($sql);
        while ($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    }

    function fetch_array($sql) {
        return mysqli_fetch_assoc($this->query($sql));
    }
    
    function getone($sql) {
        $result = $this->query($sql);
        if (mysql_num_rows($result) == 0)
            $value = false;
        else
            $value = mysql_result($result, 0);
        return $value;
    }
    
    function getOneRow($sql){ 
        $data = array();
        $result = $this->query($sql);
        $row = $result->fetch_assoc();
        return $row;
    }

    function save($sql) {
    	if(!empty($sql)){
    		mysqli_query($this->db,$sql) or die("invalid query: " . mysql_error());
    		return mysqli_insert_id($this->db);
    	}
    }
    
    
    function getCount($sql){
        $data;
        $result = $this->query($sql);
        $row = mysql_fetch_array($result);
        $data =   $row[0];
        return $data;
    }
    
    function yyyymmdd($dateOld) {
    	$explode=explode("/",$dateOld);
//    	print_r($explode);
//    	$explode[0]=substr($explode[0], -1, 2);
    	$new_date=$explode[2]."-".$explode[1]."-".$explode[0];
        return $new_date;
    }

    function ddmmyyyy($dateOld) {
    	if(!empty($dateOld)){
    		return date("d/m/Y", strtotime($dateOld));
    	} else {
    		return ;
    	}
    }
    
    function verifyEmail($email){
    	$email = $this->fetch("Select count(id) AS cnt from users where username='".$email."'");
    	$result;
    	if(intval($email[0]['cnt'])>0){
    		return true;
    	}else{
    		return false;
    	}
    }
    function close(){
    	if(isset($this->db)){
    	 mysql_close($this->db);
    	}
    }
    
    /***
     * 
     * for display limited string
     */
    function limit_words($string, $word_limit){
    	$words = explode(" ",$string);
    	return implode(" ", array_splice($words, 0, $word_limit));
    }
    
	function create_slug($string, $ext='.html'){
	        $string = strtolower($string);//convert the string to lowercase
	     $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);//strip non alpha-numeric characters
	     $string = preg_replace("/[\s-]+/", " ", $string);//remove multiple dashes or whitespaces
	     $string = preg_replace("/[\s_]/", "-", $string);//transform whitespaces and underscore to dash
	
	    return $string.$ext;
	}
	
	function select($sql){
		$data = $this->fetch($sql);
		$select = htmlentities('<option value="0">no record found</option>');
		if(count($data)>0){
			for($i=0;$i<count($data);$i++){
				$select.= htmlentities("<option value='".$data[$i]['id']."'>".$data[$i]['name']."</option>");
			}
		}
		return $select;
	}
	
	function selectWithId($sql,$selectedId){
		$data = $this->fetch($sql);
		$select = htmlentities("<option value='0'>no record found</option>");
		if(count($data)>0){
			for($i=0;$i<count($data);$i++){
				$selelected ="";
				if($selectedId == $data[$i]['id']){
					$selelected = "selected='selected'";
				}
				$select.= htmlentities("<option ".$selelected." value='".$data[$i]['id']."'>".$data[$i]['name']."</option>");
			}
		}
		return $select ;
	}
	function selectBox($tableName,$selectedId,$selectBoxName){
		$selectBox = "";
		$selectBox = "<select  class='form-control' id=".$selectBoxName." name=".$selectBoxName.">";			
		$selectBox .= "<option value='-1'>Select ".$tableName."</option>";
		$data = $this->fetch("Select * from ".$tableName);
		for($i=0;$i<count($data);$i++){
			if($selectedId==$data[$i]['id']){
				$selectBox .= "<option  selected='selected' value=".$data[$i]['id'].">".$data[$i]['name']."</option>";
			}else{
				$selectBox .= "<option value=".$data[$i]['id'].">".$data[$i]['name']."</option>";
			}
		}
		
		return  $selectBox."</select>";
	}
	
	function selectBoxByQuery($name,$query,$selectedId,$selectBoxName){
		$selectBox = "";
		$selectBox = "<select  class='form-control' id=".$selectBoxName." name=".$selectBoxName.">";
		$selectBox .= "<option value='-1'>Select ".$name."</option>";
		$data = $this->fetch($query);
		for($i=0;$i<count($data);$i++){
			if($selectedId==$data[$i]['id']){
				$selectBox .= "<option  selected='selected' value=".$data[$i]['id'].">".$data[$i]['name']."</option>";
			}else{
				$selectBox .= "<option value=".$data[$i]['id'].">".$data[$i]['name']."</option>";
			}
		}
		return  $selectBox."</select>";
	}

	/**
	 * @param $employee_id
	 * @param $month
	 * @return mixed
	 */
	function count_salary($employee_id,$month){
		$query = "SELECT salary FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$result = mysqli_query($this->db,$query);
		$row = mysqli_fetch_object($result);
		$per_day_salary = $row->salary / $this->curr_month_days();
		$monthly_salary = $row->salary;
		$total_leave = $this->get_month_total_leave($employee_id,$month);
		$leave_reduce = $per_day_salary * $total_leave;
		$monthly_salary = $monthly_salary -$leave_reduce;
		return round($monthly_salary,2);
	}

	function get_month_total_leave($employee_id,$month){
		$query = "SELECT * FROM `emp_leave` WHERE employee_id = '$employee_id' AND MONTH(timestamp) = '$month' ORDER BY id ";
		$result = mysqli_query($this->db,$query);
		$total_leave = 0;
		while($row = mysqli_fetch_object($result)){
			if($row->leave_type != 'P' && $row->leave_type != 'WH'){
				$total_leave++;
			}
		}
		return $total_leave;
	}

	function get_employee_leave_report($employee_id){
		/*echo "<pre>";*/
		$leave_report = array();

		/*---------- Employee Data ---------------*/
		$employee_query = "SELECT * FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$employee_result = mysqli_query($this->db, $employee_query);
		$employee_row = mysqli_fetch_object($employee_result);
		$employee_allotment_date = $this->get_emp_allotment_from_date($employee_id);
		$employee_avail_from_date = $this->get_emp_avail_from_date($employee_id);

		/*-------------- Leave Policy ------------------*/

		$leave_policy_query = "SELECT * FROM `company_sick_leave_policy` WHERE 1 LIMIT 1";
		$leave_policy_result = mysqli_query($this->db, $leave_policy_query);
		$leave_policy_row = mysqli_fetch_object($leave_policy_result);
		$total_leaves = $leave_policy_row->total_leaves;
		$allotment_at = $leave_policy_row->allot;  // Monthly || Quarterly || Half-Year || Yearly


		/*---------------------------------------*/
		$leave_balance = 0;
		$unpaid_leave = 0;
		$paid_leave = 0;
		$taken_sl_leave = 0;

		/*---------- Leave Data ---------------*/
		$leave_query = "SELECT * FROM `emp_leave` WHERE employee_id = '$employee_id' AND leave_date <= '2018-03-31' GROUP BY leave_date ORDER BY leave_date";
		$leave_result = mysqli_query($this->db,$leave_query);
		$dates_for_add_leave_balance = array();
		$total_days = floor((strtotime(date('Y-m-d')) - strtotime($employee_allotment_date))/3600/24);
		$allotment_interval_add_leave = 0;
		$per_day_leave = (int)$total_leaves / 365;
		if ($total_days > 0 || 1 == 1){
			if (strtolower($allotment_at) == 'monthly'){
				$allotment_interval_add_leave = (int)$total_leaves / 12;
				for($j=2016;$j < 2018;$j++){
					for($i=1;$i < 13;$i++){
						$dates_for_add_leave_balance[] = $this->month_last_date($i,$j);
					}
				}
				/*$dates_for_add_leave_balance = array(date('Y-01-01'),date('Y-02-01'),date('Y-03-01'),date('Y-04-01'),date('Y-05-01'),date('Y-06-01'),
					date('Y-07-01'),date('Y-08-01'),date('Y-09-01'),date('Y-10-01'),date('Y-11-01'),date('Y-12-01'));*/
			}else if (strtolower($allotment_at) == 'quarterly'){
				$allotment_interval_add_leave = (int)$total_leaves / 4;
				//$dates_for_add_leave_balance = array(date('Y-12-31'),date('Y-03-31'),date('Y-06-30'),date('Y-09-30'));
				for($i = date('Y');$i<date('Y') + 3;$i++){
					$dates_for_add_leave_balance[] = date("$i-12-31");
					$dates_for_add_leave_balance[] = date("$i-03-31");
					$dates_for_add_leave_balance[] = date("$i-06-30");
					$dates_for_add_leave_balance[] = date("$i-09-30");
				}
				/*$dates_for_add_leave_balance = array(date('Y-01-01'),date('Y-04-01'),date('Y-07-01'),date('Y-10-01'));*/
			} else if (strtolower($allotment_at) == 'half_yearly'){
				$allotment_interval_add_leave = (int)$total_leaves / 2;
				//$dates_for_add_leave_balance = array(date('Y-03-31'),date('Y-09-30'));
				for($i = date('Y');$i<date('Y') + 3;$i++){
					$dates_for_add_leave_balance[] = date("$i-03-31");
					$dates_for_add_leave_balance[] = date("$i-09-30");
				}
				/*$dates_for_add_leave_balance = array(date('Y-04-01'),date('Y-10-01'));*/
			}else if (strtolower($allotment_at) == 'yearly'){
				//$dates_for_add_leave_balance = array(date('Y-03-31'));
				$allotment_interval_add_leave = (int)$total_leaves;
				for($i = date('Y');$i<date('Y') + 3;$i++){
					$dates_for_add_leave_balance[] = date("$i-03-31");
				}
				/*$dates_for_add_leave_balance = array(date('Y-04-01'));*/
			}

			$month_leave = array();
			$day_counter = 1;
			while($leave_row = mysqli_fetch_object($leave_result)) {
				/*----------- if month complete ------------*/
				if($employee_allotment_date <= $leave_row->leave_date){
					/*------------ Add Leave Balance ----------------*/
					if(in_array($leave_row->leave_date,$dates_for_add_leave_balance)){ // Check date is add leave balance date or not
						if(strtolower($allotment_at) == 'monthly' && $day_counter == $this->get_month_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)))){
							$leave_balance = $leave_balance + $allotment_interval_add_leave;
							$day_counter = 0;
						}elseif(strtolower($allotment_at) == 'quarterly' && $day_counter == $this->get_quarter_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)))){
							$leave_balance = $leave_balance + $allotment_interval_add_leave;
							$day_counter = 0;
						}elseif(strtolower($allotment_at) == 'half_yearly' && $day_counter == $this->get_half_year_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)))){
							$leave_balance = $leave_balance + $allotment_interval_add_leave;
							$day_counter = 0;
						}elseif(strtolower($allotment_at) == 'yearly' && $day_counter == $this->get_year_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)))){
							$leave_balance = $leave_balance + $allotment_interval_add_leave;
							$day_counter = 0;
						}else{
							if (strtolower($allotment_at) == 'monthly'){
								$per_day_leave = $allotment_interval_add_leave / $this->get_month_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)));
							}else if (strtolower($allotment_at) == 'quarterly'){
								$per_day_leave = $allotment_interval_add_leave / $this->get_quarter_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)));
							} else if (strtolower($allotment_at) == 'half_yearly'){
								$per_day_leave = $allotment_interval_add_leave / $this->get_half_year_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)));
							}else if (strtolower($allotment_at) == 'yearly'){
								$per_day_leave = $allotment_interval_add_leave / $this->get_year_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)));
							}else{
								$per_day_leave = $allotment_interval_add_leave / $this->get_month_days(date('m',strtotime($leave_row->leave_date)),date('Y',strtotime($leave_row->leave_date)));
							}
							$leave_balance = $leave_balance + round($per_day_leave * $day_counter,2);
							$day_counter = 0;
							/*if($leave_balance > 1){
								echo "<br/>leave_date : ".$leave_row->leave_date;
								echo "<br/>leave_balance : ".$leave_balance;
								echo "<br/>day_counter : ".$day_counter;die;
							}*/
						}
					}
					$day_counter++;
				}

				if ($leave_row->leave_type == 'SL') {
					$month_leave['taken_sl_leave'][date('Ym',strtotime($leave_row->leave_date))] = isset($month_leave['taken_sl_leave'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['taken_sl_leave'][date('Ym',strtotime($leave_row->leave_date))]:0;
					$month_leave['taken_sl_leave'][date('Ym',strtotime($leave_row->leave_date))] = $month_leave['taken_sl_leave'][date('Ym',strtotime($leave_row->leave_date))] + 1;
					$taken_sl_leave++;
					if($employee_avail_from_date > $leave_row->leave_date){
						$unpaid_leave++;
						$month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))] = isset($month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))]:0;
						$month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))] = $month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))] + 1;
					}elseif($employee_avail_from_date <= $leave_row->leave_date){
						if($leave_balance >= 1){
							$leave_balance=$leave_balance-1;
							$paid_leave++;
							$month_leave['paid_leave'][date('Ym',strtotime($leave_row->leave_date))] = isset($month_leave['paid_leave'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['paid_leave'][date('Ym',strtotime($leave_row->leave_date))]:0;
							$month_leave['paid_leave'][date('Ym',strtotime($leave_row->leave_date))] = $month_leave['paid_leave'][date('Ym',strtotime($leave_row->leave_date))] + 1;
						}else{
							$unpaid_leave++;
							$month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))] = isset($month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))]:0;
							$month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))] = $month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))] + 1;
						}
					}
				}

				if ($leave_row->leave_type == 'P' || $leave_row->leave_type == 'WH') {
					$month_leave['present_days'][date('Ym',strtotime($leave_row->leave_date))] = isset($month_leave['present_days'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['present_days'][date('Ym',strtotime($leave_row->leave_date))]:0;
					$month_leave['present_days'][date('Ym',strtotime($leave_row->leave_date))] = $month_leave['present_days'][date('Ym',strtotime($leave_row->leave_date))] + 1;
				}

				$leave_report[date('Ym',strtotime($leave_row->leave_date))] = array(
					'leave_balance' => $leave_balance,
					'paid_leave' => isset($month_leave['paid_leave'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['paid_leave'][date('Ym',strtotime($leave_row->leave_date))]:0,
					'unpaid_leave' => isset($month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['unpaid_leave'][date('Ym',strtotime($leave_row->leave_date))]:0,
					'taken_sl_leave' => isset($month_leave['taken_sl_leave'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['taken_sl_leave'][date('Ym',strtotime($leave_row->leave_date))]:0,
					'present_days' => isset($month_leave['present_days'][date('Ym',strtotime($leave_row->leave_date))])?$month_leave['present_days'][date('Ym',strtotime($leave_row->leave_date))]:0,
				);
			}

		}else{
			$allotment = 0;
		}
		$leave_report['leave_balance'] = $leave_balance;
		$leave_report['unpaid_leave'] = $unpaid_leave;
		$leave_report['paid_leave'] = $paid_leave;
		$leave_report['taken_sl_leave'] = $taken_sl_leave;
		/*echo "<pre>";
		print_r($leave_report);
		die;*/
		return $leave_report;
	}


	function check_is_first_date_of_month($date,$month,$year){
		$result = strtotime("{$year}-{$month}-01");
		return strtotime($date) == strtotime(date('Y-m-d', $result))?true:false;
	}

	function check_is_last_date_of_month($date,$month,$year){
		$result = strtotime("{$year}-{$month}-01");
		$result = strtotime('-1 second', strtotime('+1 month', $result));
		return strtotime($date) == strtotime(date('Y-m-d', $result))?true:false;
	}

	function month_last_date($month = '', $year = '') {
		if (empty($month)) {
			$month = date('m');
		}
		if (empty($year)) {
			$year = date('Y');
		}
		$result = strtotime("{$year}-{$month}-01");
		$result = strtotime('-1 second', strtotime('+1 month', $result));
		return date('Y-m-d', $result);
	}

	function month_first_date($month = '', $year = '')
	{
		if (empty($month)) {
			$month = date('m');
		}
		if (empty($year)) {
			$year = date('Y');
		}
		$result = strtotime("{$year}-{$month}-01");
		return date('Y-m-d', $result);
	}

	function get_emp_allotment_from_date($employee_id)
	{
		$allotment_date = 0;

		/*------------- Fetch Employee Data ------------------------*/
		$employee_query = "SELECT * FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$employee_result = mysqli_query($this->db, $employee_query);
		$employee_row = mysqli_fetch_object($employee_result);
		$date_of_joining = $employee_row->dateofjoinin;
		$confirmation_date = $employee_row->confdate;
		$prob_date = $employee_row->prodate;

		/*---------- Fetch Sick Leave Policy ----------------------*/
		$leave_policy_query = "SELECT * FROM `company_sick_leave_policy` WHERE 1 LIMIT 1";
		$leave_policy_result = mysqli_query($this->db, $leave_policy_query);
		$leave_policy_row = mysqli_fetch_object($leave_policy_result);
		$allot_from = $leave_policy_row->allot_from;
		$allot_after_month = $leave_policy_row->allot_after_month;
		$allot_after_days = $leave_policy_row->allot_after_days;

		if (strtolower($allot_from) == 'doj') { // Date of joining
			$allotment_date = $date_of_joining;
		} elseif (strtolower($allot_from) == 'pd') { // Prob. period
			$allotment_date = $prob_date;
		} elseif (strtolower($allot_from) == 'cd') { // Conformation date
			$allotment_date = $confirmation_date;
		}

		$allotment_date_string = strtotime($allotment_date);
		if ($allot_after_month && $allot_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $allot_after_month . " month", $allotment_date_string)));
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $allot_after_days . " day", $allotment_date_string)));
		} else if ($allot_after_month && !$allot_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $allot_after_month . " month", $allotment_date_string)));
		} else if (!$allot_after_month && $allot_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $allot_after_days . " day", $allotment_date_string)));
		}
		$allotment_date = date("Y-m-d", $allotment_date_string);
		return $allotment_date;
	}

	function get_emp_avail_from_date($employee_id)
	{
		$avail_from_date = 0;

		/*------------- Fetch Employee Data ------------------------*/
		$employee_query = "SELECT * FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$employee_result = mysqli_query($this->db, $employee_query);
		$employee_row = mysqli_fetch_object($employee_result);
		$date_of_joining = $employee_row->dateofjoinin;
		$confirmation_date = $employee_row->confdate;
		$prob_date = $employee_row->prodate;

		/*---------- Fetch Sick Leave Policy ----------------------*/
		$leave_policy_query = "SELECT * FROM `company_sick_leave_policy` WHERE 1 LIMIT 1";
		$leave_policy_result = mysqli_query($this->db, $leave_policy_query);
		$leave_policy_row = mysqli_fetch_object($leave_policy_result);
		$avail_from = $leave_policy_row->avail_from;
		$avail_after_month = $leave_policy_row->avail_after_month;
		$avail_after_days = $leave_policy_row->avail_after_days;

		if (strtolower($avail_from) == 'doj') { // Date of joining
			$avail_from_date = $date_of_joining;
		} elseif (strtolower($avail_from) == 'pd') { // Prob. period
			$avail_from_date = $prob_date;
		} elseif (strtolower($avail_from) == 'cd') { // Conformation date
			$avail_from_date = $confirmation_date;
		}

		$allotment_date_string = strtotime($avail_from_date);
		if ($avail_after_month && $avail_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $avail_after_month . " month", $allotment_date_string)));
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $avail_after_days . " day", $allotment_date_string)));
		} else if ($avail_after_month && !$avail_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $avail_after_month . " month", $allotment_date_string)));
		} else if (!$avail_after_month && $avail_after_days) {
			$allotment_date_string = strtotime(date("Y-m-d", strtotime("+" . $avail_after_days . " day", $allotment_date_string)));
		}
		$avail_from_date = date("Y-m-d", $allotment_date_string);
		return $avail_from_date;
	}

	/**
	 * @param string $month
	 * @param string $year
	 * @return int
	 */
	function get_month_days($month = '',$year = ''){
		$month = $month!=''?$month:date('m');
		$year = $year!=''?$year:date('Y');
		return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
	}

	/**
	 * @param string $month
	 * @param string $year
	 * @return int
	 */
	function get_quarter_days($month = '',$year = ''){
		$first_month = $month!=''?$month:date('m');
		$second_month = $first_month == 1?12:$first_month-1;
		$third_month = $second_month-1;
		$year = $year!=''?$year:date('Y');

		$first_month_days = $first_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($first_month - 1) % 7 % 2 ? 30 : 31);

		$year = $first_month==1?$year-1:$year;
		$second_month_days = $second_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($second_month - 1) % 7 % 2 ? 30 : 31);

		$third_month_days = $third_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($third_month - 1) % 7 % 2 ? 30 : 31);

		$total_days = $first_month_days + $second_month_days + $third_month_days;
		return $total_days;
	}

	/**
	 * @param string $month
	 * @param string $year
	 * @return int
	 */
	function get_half_year_days($month = '',$year = ''){
		$year = $year!=''?$year:date('Y');

		$first_month = $month!=''?$month:date('m');
		$first_month_days = $first_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($first_month - 1) % 7 % 2 ? 30 : 31);

		$year = $first_month==1?$year-1:$year;
		$second_month = $first_month == 1?12:$first_month-1;
		$second_month_days = $second_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($second_month - 1) % 7 % 2 ? 30 : 31);

		$year = $second_month==1?$year-1:$year;
		$third_month = $second_month == 1?12:$second_month-1;
		$third_month_days = $third_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($third_month - 1) % 7 % 2 ? 30 : 31);

		$year = $third_month==1?$year-1:$year;
		$forth_month = $third_month == 1?12:$third_month-1;
		$forth_month_days = $forth_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($forth_month - 1) % 7 % 2 ? 30 : 31);

		$year = $forth_month==1?$year-1:$year;
		$fifth_month = $forth_month == 1?12:$forth_month-1;
		$fifth_month_days = $fifth_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($fifth_month - 1) % 7 % 2 ? 30 : 31);

		$year = $fifth_month==1?$year-1:$year;
		$six_month = $fifth_month == 1?12:$fifth_month-1;
		$six_month_days = $six_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($six_month - 1) % 7 % 2 ? 30 : 31);


		$total_days = $first_month_days + $second_month_days + $third_month_days + $forth_month_days + $fifth_month_days + $six_month_days;
		return $total_days;
	}

	function get_year_days($month = '',$year = ''){
		$year = $year!=''?$year:date('Y');

		$first_month = $month!=''?$month:date('m');
		$first_month_days = $first_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($first_month - 1) % 7 % 2 ? 30 : 31);

		$year = $first_month==1?$year-1:$year;
		$second_month = $first_month == 1?12:$first_month-1;
		$second_month_days = $second_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($second_month - 1) % 7 % 2 ? 30 : 31);

		$year = $second_month==1?$year-1:$year;
		$third_month = $second_month == 1?12:$second_month-1;
		$third_month_days = $third_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($third_month - 1) % 7 % 2 ? 30 : 31);

		$year = $third_month==1?$year-1:$year;
		$forth_month = $third_month == 1?12:$third_month-1;
		$forth_month_days = $forth_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($forth_month - 1) % 7 % 2 ? 30 : 31);

		$year = $forth_month==1?$year-1:$year;
		$fifth_month = $forth_month == 1?12:$forth_month-1;
		$fifth_month_days = $fifth_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($fifth_month - 1) % 7 % 2 ? 30 : 31);

		$year = $fifth_month==1?$year-1:$year;
		$six_month = $fifth_month == 1?12:$fifth_month-1;
		$six_month_days = $six_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($six_month - 1) % 7 % 2 ? 30 : 31);

		$year = $six_month==1?$year-1:$year;
		$seventh_month = $six_month == 1?12:$six_month-1;
		$seventh_month_days = $seventh_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($seventh_month - 1) % 7 % 2 ? 30 : 31);

		$year = $seventh_month==1?$year-1:$year;
		$eight_month = $seventh_month == 1?12:$seventh_month-1;
		$eight_month_days = $eight_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($eight_month - 1) % 7 % 2 ? 30 : 31);

		$year = $eight_month==1?$year-1:$year;
		$ninth_month = $eight_month == 1?12:$eight_month-1;
		$ninth_month_days = $ninth_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($ninth_month - 1) % 7 % 2 ? 30 : 31);

		$year = $ninth_month==1?$year-1:$year;
		$tenth_month = $ninth_month == 1?12:$ninth_month-1;
		$tenth_month_days = $tenth_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($tenth_month - 1) % 7 % 2 ? 30 : 31);

		$year = $tenth_month==1?$year-1:$year;
		$eleventh_month = $tenth_month == 1?12:$tenth_month-1;
		$eleventh_month_days = $eleventh_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($eleventh_month - 1) % 7 % 2 ? 30 : 31);

		$year = $eleventh_month==1?$year-1:$year;
		$twelve_month = $eleventh_month == 1?12:$eleventh_month-1;
		$twelve_month_days = $twelve_month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($twelve_month - 1) % 7 % 2 ? 30 : 31);


		$total_days = $first_month_days + $second_month_days + $third_month_days + $forth_month_days + $fifth_month_days + $six_month_days + $seventh_month_days + $eight_month_days + $ninth_month_days + $tenth_month_days + $eleventh_month_days + $twelve_month_days;
		return $total_days;
	}

	function get_leave_report($employee_id){

		/*---------- Employee Data ---------------*/
		$employee_query = "SELECT * FROM `employee` WHERE id = '$employee_id' LIMIT 1";
		$employee_result = mysqli_query($this->db, $employee_query);
		$employee_row = mysqli_fetch_object($employee_result);
		$date_of_joining = $employee_row->dateofjoinin;
		$confirmation_date = $employee_row->confdate;
		$prob_date = $employee_row->prodate;

		/*-------------- Leave Policy ------------------*/

		$leave_policy_query = "SELECT * FROM `company_sick_leave_policy` WHERE 1 LIMIT 1";
		$leave_policy_result = mysqli_query($this->db, $leave_policy_query);
		$leave_policy_row = mysqli_fetch_object($leave_policy_result);
		$allot_from = $leave_policy_row->allot_from;
		$allot_after_month = $leave_policy_row->allot_after_month;
		$allot_after_days = $leave_policy_row->allot_after_days;
		$total_leaves = $leave_policy_row->total_leaves;
		$allotment_at = $leave_policy_row->allot;  // Monthly || Quarterly || Half-Year || Yearly

		$query = "SELECT YEAR(leave_date) as year, MONTH(leave_date) as month, SUM(CASE WHEN `leave_type` NOT IN('P','WH') THEN 1 ELSE 0 END) as taken_leave FROM `emp_leave` WHERE employee_id = '11' GROUP BY YEAR(leave_date),MONTH(leave_date)";
		$leaves = $this->fetch($query);
		if(count($leaves) > 0){

			$remaining_leave = 0;
			$remaining_first_leave_iteration = 0;
			foreach($leaves as $leave){
				$paid_leave = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['paid_leave'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['paid_leave']:0;
				$unpaid_leave = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['unpaid_leave'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['unpaid_leave']:0;
				$leave_balance = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['leave_balance'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['leave_balance']:0;

				//naitik edited code
				//$date = new DateTime('2016-04-01');//take user joining/probestion/confirm date as per given policy
				if (strtolower($allot_from) == 'doj') { // Date of joining
					$date = $date_of_joining;
				} elseif (strtolower($allot_from) == 'pd') { // Prob. period
					$date = $prob_date;
				} elseif (strtolower($allot_from) == 'cd') { // Conformation date
					$date = $confirmation_date;
				}
				//Add dynamic months and day if any in leave policy
				//if user has month and days then add like P<Month>M<Days>D as perr leave policy configuration
				// visit link http://php.net/manual/en/datetime.add.php
				//$dynamic_day_month = "P0M15D";//take dyanmic month and days as per emp_policy and add acordingly
				$dynamic_day_month = "P".$allot_after_month."M".$allot_after_days."D";//take dyanmic month and days as per emp_policy and add acordingly
				$date->add(new DateInterval($dynamic_day_month));
				//echo $date->format('Y-m-d H:i:s') . "\n";

				//Calculate monthly leave as per policy and yearly leave
				//yearly 24 then onthly 2
				$yearly_leave = $total_leaves;//Make this value dynamic
				$monthly_leave = $yearly_leave /12;

				//calculate remaining monthly days
				$timestamp = strtotime($date->format('Y-m-d H:i:s'));
				$daysRemaining = (int)date('t', $timestamp) - (int)date('j', $timestamp);//curent month

				//echo ($daysRemaining/30)*$monthly_leave;

				$start_month=date('m', strtotime( $date->format('Y-m-d H:i:s')));
				$end_month =str_pad($leave['month'],2,'0',STR_PAD_LEFT);
				//echo $start_month."--".$end_month;
				if($start_month==$end_month && $remaining_first_leave_iteration == 0)
				{
					$remaining_first_leave_iteration =1;
					$remaining_leave = $remaining_leave+($daysRemaining/30)*$monthly_leave;
					//echo "First time";
				}
				else
				{
					//echo "iteration time";
					//$remaining_leave+=$monthly_leave+ ;
					$remaining_leave = $remaining_leave+$monthly_leave;
				}
				//echo $remaining_leave;
				$present_days = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['present_days'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['present_days']:0;
				$remaining_leave = $remaining_leave - $paid_leave;
			}
		}
	}
}
?>