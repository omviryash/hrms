CREATE TABLE `emp_leave_calc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `emp_closing_balance_SL` int(11) NOT NULL,
  `emp_closing_balance_CO` int(11) NOT NULL,
  `emp_allotment_SL` int(11) NOT NULL,
  `emp_allotment_CO` int(11) NOT NULL,
  `emp_availed_SL` int(11) NOT NULL,
  `emp_availed_CO` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;