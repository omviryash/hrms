<?php include_once 'menu.php';?>
<?php
if(isset($_POST['btn_add_leave'])){
	$employee_id = isset($_POST['employee_id'])?$_POST['employee_id']:11;
	$leave_type = isset($_POST['leave_type'])?$_POST['leave_type']:'SL';
	if(isset($_POST['leave_date'])){
		$leave_date = $_POST['leave_date'];
		$leave_date = explode(' - ',$leave_date);
		$starDate = $leave_date[0];
		$endDate  = $leave_date[1];
		$companyPolicySql = "select * from company_sick_leave_policy";
		$companyPolicyData = $db->fetch($companyPolicySql);
		if (count($companyPolicyData) > 0) {
			$WH_day_str = " " . $companyPolicyData[0]['select_week_off'];
			if ($starDate == $endDate) {
				$emp_id = isset($_POST['emp_id'])?$_POST['emp_id']:11;
				$starDate = date('Y-m-d',strtotime(str_replace('/','-',$starDate)));
				$leave_query = "SELECT * FROM `emp_leave` WHERE leave_date = '$starDate' AND employee_id = '$employee_id'";
				$leave_result = $db->query($leave_query);
				$dayName = strtolower(date('D', strtotime($starDate)));
				$WHCheck = strpos($WH_day_str,$dayName);
				if(mysqli_num_rows($leave_result) > 0){
					if ($WHCheck === false) {
						$db->query("UPDATE emp_leave SET leave_type='$leave_type' WHERE leave_date = '$starDate' AND employee_id = '$employee_id'");
					}else{
						$db->query("UPDATE emp_leave SET leave_type='WH' WHERE leave_date = '$starDate' AND employee_id = '$employee_id'");
					}
				}else{
					if ($WHCheck === false) {
						$db->query("insert into emp_leave(leave_type,leave_date,employee_id) value('" . $leave_type . "','" . $starDate . "'," . $employee_id . ")");
					}else{
						$db->query("insert into emp_leave(leave_type,leave_date,employee_id) value('WH','" . $starDate . "'," . $employee_id . ")");
					}
				}
			} else {
				$starDate = str_replace('/','-',$starDate);
				$endDate = str_replace('/','-',$endDate);
				for ($i = date('Y-m-d',strtotime($starDate)); $i <= date('Y-m-d',strtotime($endDate)); $i = date('Y-m-d',strtotime('+1 day',strtotime($i)))) {
					$leave_date = date('Y-m-d',strtotime($i));
					$leave_query = "SELECT * FROM `emp_leave` WHERE leave_date = '$leave_date' AND employee_id = '$employee_id'";
					$leave_result = $db->query($leave_query);
					$dayName = strtolower(date('D',strtotime($i)));
					$WHCheck = strpos($WH_day_str,$dayName);
					if(mysqli_num_rows($leave_result) > 0){
						if ($WHCheck === false) {
							$db->query("UPDATE emp_leave SET leave_type='$leave_type' WHERE leave_date = '$leave_date' AND employee_id = '$employee_id'");
						} else {
							$db->query("UPDATE emp_leave SET leave_type='WH' WHERE leave_date = '$leave_date' AND employee_id = '$employee_id'");
						}
					}else{
						if ($WHCheck === false) {
							$db->query("insert into emp_leave(leave_type,leave_date,employee_id) value('" . $leave_type . "','" . $leave_date. "'," . $employee_id . ")");
						} else {
							$sql = "insert into emp_leave(leave_type,leave_date,employee_id) value('WH','" .$leave_date . "'," . $employee_id . ")";
							$db->save($sql);
						}
					}
				}
			}
		}
	}

}
?>
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Employee Leave Details </h1>
			<ol class="breadcrumb">
				<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Employee Leave Details</li>
			</ol>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Employee Leave Information</h3>
						</div>
						<form role="form" method="post" action="add_leave.php">
							<div class="box-body">
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputEmail1">Employee Name:</label>
										<select  class="form-control" name="employee_id">
										<?php
											$sql = "Select * from employee";
											$data = $db->fetch($sql);
											for($i=0;$i<count($data);$i++){
												echo "<option value=".$data[$i]['id'].">".$data[$i]['empname']."</option>";
											}
										?>
										</select>
									</div>
									<!-- <div class="form-group">
										<label for="exampleInputPassword1">Employee Code : </label>
										<span class="form-control">CLIF-001</span>
									</div> -->
									<div class="form-group">
										<label for="exampleInputEmail1">Leave Type</label>
										<select  class="form-control" id="leave_type" name="leave_type">
											<option value="P">P</option>
											<option value="LOP">LOP</option>
											<option value="SL">SL</option>
											<option value="CL">CL</option>
											<option value="PL">PL</option>
											<option value="CO">CO</option>
											<option value="CW">CW</option>
											<option value="12P">1/2P</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputPassword1">Leave Date</label>
										<input type="text" placeholder="Select Confirmation Date" id="leave_date" name="leave_date" class="form-control">
									</div>
								</div>
 							</div>
							<div class="box-footer">
								<button class="btn btn-primary" type="submit" name="btn_add_leave">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Employee Leave Information</h3>
						</div>
						<div class="box-body">
							<table class="table table-border">
								<thead>
								<tr>
									<th>Month</th>
									<th>Leave</th>
									<?php
									for($i = 1;$i < 32;$i++) {
										?>
										<th><?=$i?></th>
										<?php
									}
									?>
								</tr>
								</thead>
								<tbody>
								<?php
								$month_query = "SELECT YEAR(leave_date) as year,DATE_FORMAT(leave_date,'%m') as month, SUM(CASE WHEN `leave_type` NOT IN('P','WH') THEN 1 ELSE 0 END) as taken_leave FROM `emp_leave` WHERE employee_id = '11' GROUP BY CONCAT(YEAR(leave_date),DATE_FORMAT(leave_date,'%m')) ORDER BY CONCAT(YEAR(leave_date),DATE_FORMAT(leave_date,'%m'))";
								$month_result = $db->query($month_query);
								if(mysqli_num_rows($month_result) > 0) {
									while($month_row = mysqli_fetch_object($month_result)){
										?>
										<tr>
											<td><?=$month_row->year.'-'.$month_row->month?></td>
											<td><?=$month_row->taken_leave?></td>
											<?php
											$year_month = $month_row->year.$month_row->month;
											$day_query = "SELECT * FROM `emp_leave` WHERE employee_id = '11' AND CONCAT(YEAR(leave_date),DATE_FORMAT(leave_date,'%m')) = '$year_month' ORDER BY leave_date ASC";
											$day_result = $db->query($day_query);
											$day_counter = 1;
											while($day_row = mysqli_fetch_object($day_result)){
												echo "<td>".$day_row->leave_type."</td>";
												$day_counter++;
											}
											while($day_counter < 32){
												echo "<td>-</td>";
												$day_counter++;
											}
											?>
										</tr>
								<?php
									}
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<footer class="main-footer">
		<div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div>
		<strong>Copyright &copy; 2016-2017<a href="#"> &nbsp;OM</a>.</strong> All rights reserved.
	</footer>
	<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="plugins/morris/morris.min.js"></script> -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$.widget.bridge('uibutton', $.ui.button);
	$(function () {
		$('#leave_date').daterangepicker({
			"locale": {
				"format": "DD/MM/YYYY"
			}
		});
	});
</script>
</body>
</html>
                                        