<?php include_once 'menu.php'; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1> Leave Report </h1>
        <ol class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Leave Report</li>
        </ol>
    </section>
    <?php
    if (isset($_POST) && isset($_POST['month_filter']) && isset($_POST['year_filter'])) {
        $dayColumns = 0;
        if ($_POST['month_filter'] != "" && $_POST['year_filter'] != "") {
            $dayColumns = cal_days_in_month(CAL_GREGORIAN, $_POST['hidden_month_filter'], $_POST['year_filter']);
            $sql = "Select * from employee";
            $empData = $db->fetch($sql);
        }

        $companyPolicyData = getCompanyPolicy();
        /*print_r($companyPolicyData);die;*/
    }
    ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filter Leave Report</h3>
                        <div class="report-filter-wrapper">
                            <form name="report-filter-form" method="post">
                                <div class="filter-month form-group col-md-2">
                                    <label>Select Month:</label>
                                    <input type="text" id="month_filter" name="month_filter"
                                           class="form-control pull-right"
                                           value="<?php echo (isset($_POST['month_filter'])) ? $_POST['month_filter'] : ''; ?>">
                                    <input type="hidden" id="hidden_month_filter" name="hidden_month_filter"
                                           class="form-control pull-right"
                                           value="<?php echo (isset($_POST['hidden_month_filter'])) ? $_POST['hidden_month_filter'] : ''; ?>">
                                </div>
                                <div class="filter-year form-group col-md-2">
                                    <label>Select Year:</label>
                                    <input type="text" id="year_filter" name="year_filter"
                                           class="form-control pull-right"
                                           value="<?php echo (isset($_POST['year_filter'])) ? $_POST['year_filter'] : ''; ?>">
                                </div>
                                <div class="filter-submit form-group col-md-2">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-primary" type="submit">Generate Report</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leave Report</h3>
                        <div class="leave-report-wrapper">
                            <div class="table-responsive">
                                <table id="leave-report-table" class="display table table-striped" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <?php
                                        if (isset($dayColumns) && $dayColumns > 0) {
                                            for ($i = 1; $i <= $dayColumns; $i++) {
                                                ?>
                                                <th></th>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <th colspan="2">Opening Balance</th>
                                        <th colspan="2">Allotment</th>
                                        <th colspan="2">Availed</th>
                                        <th colspan="2">Closing</th>
                                    </tr>
                                    <tr>
                                        <th>Sr. no</th>
                                        <th>Ee.Code</th>
                                        <th>Ee.Name</th>
                                        <th>Salary</th>
                                        <th>Designation</th>
                                        <th>Start date</th>
                                        <th>Pay Days</th>
                                        <th>Present Days</th>
                                        <?php
                                        if (isset($dayColumns) && $dayColumns > 0) {
                                            for ($i = 1; $i <= $dayColumns; $i++) {
                                                ?>
                                                <th><?php echo $_POST['hidden_month_filter'] . "/" . $i . "/" . $_POST['year_filter'] ?></th>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <th>SL</th>
                                        <th>CO</th>
                                        <th>SL</th>
                                        <th>CO</th>
                                        <th>SL</th>
                                        <th>CO</th>
                                        <th>SL</th>
                                        <th>CO</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (isset($empData)) {
                                        foreach ($empData as $empLeaveData) {
                                            $leave_type_data = getEmpLeaveByDateRange($empLeaveData['id'], $_POST['hidden_month_filter'], $_POST['year_filter']);
                                            if (isset($leave_type_data) and count($leave_type_data) > 0) {
                                                $paidDays = 0;
                                                $presentDays = 0;
                                                foreach ($leave_type_data as $leaveData) {
                                                    if ($leaveData['leave_type'] != "" && $leaveData['leave_type'] != "N/A" && $leaveData['leave_type'] != "lop" && $leaveData['leave_type'] != "LWP") {
                                                        $paidDays = $paidDays + 1;
                                                    }

                                                    if ($leaveData['leave_type'] == "P") {
                                                        $presentDays = $presentDays + 1;
                                                    }
                                                }

                                                ?>
                                                <tr>
                                                    <td><?php echo $empLeaveData['id']; ?></td>
                                                    <td><?php echo $empLeaveData['empcode']; ?></td>
                                                    <td><?php echo $empLeaveData['empname']; ?></td>
                                                    <td><?php echo $db->count_salary($empLeaveData['id'],$_POST['hidden_month_filter']); ?></td>
                                                    <td><?php echo $empLeaveData['designation']; ?></td>
                                                    <td><?php echo $empLeaveData['dateofjoinin']; ?></td>
                                                    <td><?php echo $paidDays; ?></td>
                                                    <td><?php echo $presentDays; ?></td>
                                                    <?php
                                                    if (isset($dayColumns) && $dayColumns > 0) {
                                                        for ($i = 1; $i <= $dayColumns; $i++) {
                                                            $submitDateSring = strtotime($_POST['hidden_month_filter'] . "/" . $i . "/" . $_POST['year_filter']);
                                                            $submitDateRange = date("Y-m-d", $submitDateSring);
                                                            $LeaveDataKey = array_search($submitDateRange, array_column($leave_type_data, 'leave_date'));
                                                            if ($LeaveDataKey !== NULL) {
                                                                $leaveType = $leave_type_data[$LeaveDataKey]['leave_type'];
                                                            } else {
                                                                $leaveType = "N/A";
                                                            }
                                                            ?>
                                                            <td><?php echo $leaveType; ?></td>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <td>N/A</td>
                                                    <td>N/A</td>
                                                    <td><?php echo getAllotmentSL($empLeaveData, $companyPolicyData, $dayColumns, $paidDays, $presentDays); ?></td>
                                                    <td>N/A</td>
                                                    <td>N/A</td>
                                                    <td>N/A</td>
                                                    <td>N/A</td>
                                                    <td>N/A</td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div>
    <strong>Copyright &copy; 2016-2017<a href="#"> &nbsp;OM</a>.</strong> All rights reserved.
</footer>
<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="plugins/morris/morris.min.js"></script> -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function () {

        $("#month_filter").datepicker(
            {
                format: "MM",
                startView: 1,
                minViewMode: 1,
                maxViewMode: 1,
                autoclose: true
            }).on('changeDate', function () {
            $('#hidden_month_filter').val($('#month_filter').datepicker('getFormattedDate', 'm'));
        });

        $("#year_filter").datepicker(
            {
                autoclose: true,
                format: "yyyy",
                startView: 2,
                minViewMode: 2,
                maxViewMode: 2
            });
        $('#leave-report-table').DataTable();

    });

</script>
</body>
</html>