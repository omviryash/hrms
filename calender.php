	<?php include_once 'menu.php';?>
	<script>
		function validateForm(){
			var flag = true;
			var holidayName = $('#holidayname').val().trim();
			var holidayDate = $('#holidayDate').val().trim();
			
			if(holidayName.length==0){
				alert('Please enter holiday name');
				flag = false;
			}
			if(holidayDate.length==0){
				alert('Please enter holiday date');
				flag = false;
			}
			
			return flag;
		}
	</script>
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Company Calender</h1>
			<ol class="breadcrumb">
				<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Company Calender Details</li>
			</ol>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Company Calender Information</h3>
						</div>
						<form method="post" action="calender.php" onsubmit="return validateForm();">
							<div class="box-body">
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputEmail1">Year :</label>
										<span class="form-control"><?php echo date('Y')?></span>
									</div>
									<div class="form-group">
										<label for="exampleInputPassword1">Holiday Name : </label>
										<input type="text" placeholder="Enter Holiday Name" id="holidayname" class="form-control" name="holidayname">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
									<div class="form-group">
										<label for="exampleInputPassword1">Holiday Date :</label>
										<input type="text" placeholder="Select Holiday Date" id="holidayDate" class="form-control" readonly="readonly" name="holidaydate">
									</div>
									</div>
								</div>
 							</div>
							<div class="box-footer">
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Company Calender Year : </h3>
				</div>
				<div class="box-body">
					<?php 
						if(!empty($_POST)){
							$sql = "insert into calender(year,holidaydate,holidayname) values('".date("Y")."','".$db->yyyymmdd($_POST['holidaydate'])."','".$_POST['holidayname']."')";
							$db->save($sql);
							header('Location: ' . basename($_SERVER['PHP_SELF']));
						}
					?>
					<table id="example1AA" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Sr</th>
								<th>Holiday Name</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							$sql = "Select * from calender where year = '".date('Y')."'";
							$data = $db->fetch($sql);
							for($i=0;$i<count($data);$i++){
								echo "<tr><td>".($i+1)."</td><td>".$data[$i]['holidayname']."</td><td>".$db->ddmmyyyy($data[$i]['holidaydate'])."<td></tr>";
							}
						?>
  						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
	<footer class="main-footer">
		<div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div>
		<strong>Copyright &copy; 2016-2017<a href="#"> &nbsp;OM</a>.</strong> All rights reserved.
	</footer>
	<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="plugins/morris/morris.min.js"></script> -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$.widget.bridge('uibutton', $.ui.button);
	$(function () {
		$("#example1").DataTable();
		$('#holidayDate').datepicker({
			autoclose: true,
			format:'dd/mm/yyyy'
		});
	
	});
</script>
</body>
</html>
<?php ob_flush();?>