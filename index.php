<?php include_once 'menu.php';?>
	<script>
		function validateForm(){
			var flag = true;
			var ename = $('#ename').val().trim();
			var dob = $('#dob').val().trim();
			var probDate = $('#probdate').val().trim();
			var confdate= $('#confdate').val().trim();
			var designation = $('#designation').val().trim();
			
			if(ename.length==0){
				alert('Please enter name of the employee');
				flag = false;
			}
			if(dob.length==0){
				alert('Please select birthdate ');
				flag = false;
			}
			if(probDate.length==0){
				alert('Please select prob date');
				flag = false;
			}
			if(confdate.length==0){
				alert('Please select conf date');
				flag = false;
			}
			if(designation.length==0){
				alert('Please select designation');
				flag = false;
			}
			return flag;
		}
	</script>
	<div class="content-wrapper">
		<section class="content-header">
			<h1>  Employee Details </h1>
			<ol class="breadcrumb">
				<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Employee Details</li>
			</ol>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Employee Information</h3>
						</div>
						<form role="form"  method="post" action ="index.php" onsubmit="return validateForm();">
							<div class="box-body">
								<div class="col-md-4">
									<div class="form-group">
										<label for="exampleInputEmail1">Employee Name:</label>
										<input type="text" placeholder="Enter Employee Name" id="ename" name="ename" class="form-control">
									</div>
									<div class="form-group">
										<label for="exampleInputPassword1">Date of joing:</label>
										<input type="text" placeholder="Select Date Of Joing" id="dob" name="dob" class="form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="exampleInputEmail1">Prob.Date:</label>
										<input type="text" placeholder="Select Prob Date" id="probdate" name="probdate" class="form-control">
									</div>
									<div class="form-group">
										<label for="exampleInputPassword1">Confirmation Date:</label>
										<input type="text" placeholder="Select Confirmation Date" id="confdate" name="confdate" class="form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="exampleInputEmail1">Designation:</label>
										<input type="text" placeholder="Enter Designation" id="designation" name="designation" class="form-control">
									</div>
									<div class="form-group">
										<label for="exampleInputPassword1">Gender:</label>
										<select  class="form-control" name="gender" id="gender">
											<option>Male</option>
											<option>Female</option>
										</select>
									</div>
								</div>
							</div>
							<div class="box-footer">
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">List of Employees</h3>
				</div>
				<div class="box-body">
				<?php 
				if(!empty($_POST)){
					$sql = "insert into employee(empname,dateofjoinin,prodate,confdate,designation,gender) "; 
					$sql = $sql ." values('".$_POST['ename']."','".$db->yyyymmdd($_POST['dob'])."','".$db->yyyymmdd($_POST['probdate'])."','".$db->yyyymmdd($_POST['confdate'])."','".$_POST['designation']."','".$_POST['gender']."')";
					$db->save($sql);
					header('Location: ' . basename($_SERVER['PHP_SELF']));
				}
				?>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Sr.No</th>
								<th>Ee.Code</th>
								<th>Ee.Name</th>
								<th>DOJ</th>
								<th>Prob.Date</th>
								<th>Confirmation Date</th>
								<th>Designation</th>
								<th>Gender</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							$sql = "Select * from employee";
							$data = $db->fetch($sql);
							for($i=0;$i<count($data);$i++){
								echo "<tr><td>".($i+1)."</td><td></td><td>".$data[$i]['empname']."</td><td>".$db->ddmmyyyy($data[$i]['dateofjoinin'])."</td><td>".$db->ddmmyyyy($data[$i]['prodate'])."</td><td>".$db->ddmmyyyy($data[$i]['confdate'])."</td><td>".$data[$i]['designation']."</td><td>".$data[$i]['gender']."</td></tr>";
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<th>Sr.No</th>
								<th>Ee.Code</th>
								<th>Ee.Name</th>
								<th>DOJ</th>
								<th>Prob.Date</th>
								<th>Confirmation Date</th>
								<th>Designation</th>
								<th>Gender</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</section>
	</div>
	<footer class="main-footer">
		<div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div>
		<strong>Copyright &copy; 2016-2017<a href="#"> &nbsp;OM</a>.</strong> All rights reserved.
	</footer>
	<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="plugins/morris/morris.min.js"></script> -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$.widget.bridge('uibutton', $.ui.button);
	$(function () {
		$("#example1").DataTable();
		
		$('#dob').datepicker({
			autoclose: true,
			format:'dd/mm/yyyy'
		}).on('changeDate', function(ev){
			
			var probdate = $('#dob').datepicker('getDate', '+30d'); 
		  probdate.setDate(probdate.getDate() + 30); 
		  $('#probdate').datepicker('setDate', probdate);
		  
		  var confdate = $('#probdate').datepicker('getDate', '+31d'); 
		  confdate.setDate(confdate.getDate() + 30); 
		  $('#confdate').datepicker('setDate', confdate);
		  
		});
		
		$('#probdate,#confdate').datepicker({
			autoclose: true,
			format:'dd/mm/yyyy'
		});
		
	});
</script>
</body>
</html>
