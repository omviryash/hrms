<?php include_once 'menu.php'; ?>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Employee Detail</h3>
                    </div>
                    <div class="box-body">
                        <?php
                           // $emp_id = isset($_POST['emp_id'])?$_POST['emp_id']:11;
                            $emp_id = 11;
                            $employee_data = getEmpById($emp_id);
                        ?>
                        <div class="col-md-12">
                            <div class="col-md-2">Employee name</div>
                            <div class="col-md-8"><?=$employee_data['empname']?></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">Date of joining</div>
                            <div class="col-md-8"><?=$employee_data['dateofjoinin']?></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">Prob. Period</div>
                            <div class="col-md-8"><?=$employee_data['prodate']?></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">Conformation date</div>
                            <div class="col-md-8"><?=$employee_data['confdate']?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leave Report</h3>
                        <div class="leave-report-wrapper">
                            <div class="table-responsive">
                                <?php
                                    if(isset($_POST['emp_id']) || 1 == 1) {
                                        $emp_id = isset($_POST['emp_id'])?$_POST['emp_id']:11;
                                        $query = "SELECT YEAR(leave_date) as year, MONTH(leave_date) as month, SUM(CASE WHEN `leave_type` NOT IN('P','WH') THEN 1 ELSE 0 END) as taken_leave FROM `emp_leave` WHERE employee_id = '11' GROUP BY YEAR(leave_date),MONTH(leave_date)";
                                        $leaves = $db->fetch($query);
                                        $db = new db_connect();
                                        $leave_reports_data = $db->get_employee_leave_report(11)
                                        ?>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Month</th>
                                                    <th>Taken</th>
                                                    <th>Paid</th>
                                                    <th>Without paid</th>
                                                    <th>Leave balance</th>
                                                    <th>Present Days</th>
                                                    <th>Salary</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($leaves) > 0){
													$remaining_leave = 0;
													$remaining_first_leave_iteration = 0;
                                                    foreach($leaves as $leave){
                                                        $paid_leave = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['paid_leave'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['paid_leave']:0;
                                                        $unpaid_leave = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['unpaid_leave'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['unpaid_leave']:0;
                                                        $leave_balance = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['leave_balance'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['leave_balance']:0;
                                                        //naitik edited code
														$date = new DateTime('2016-04-01');//take user joining/probestion/confirm date as per given policy
														//Add dynamic months and day if any in leave policy
														//if user has month and days then add like P<Month>M<Days>D as perr leave policy configuration
														// visit link http://php.net/manual/en/datetime.add.php
														$dynamic_day_month = 'P0M15D';//take dyanmic month and days as per emp_policy and add acordingly
														$date->add(new DateInterval($dynamic_day_month));
														//echo $date->format('Y-m-d H:i:s') . "\n";
														
														//Calculate monthly leave as per policy and yearly leave
														//yearly 24 then onthly 2
														$yearly_leave = 24;//Make this value dynamic
														$monthly_leave = $yearly_leave /12;
														
														//calculate remaining monthly days
                                                        $timestamp = strtotime($date->format('Y-m-d H:i:s'));
														$daysRemaining = 1 + (int)date('t',$timestamp) - (int)date('j', $timestamp);//curent month
														/*print_r($daysRemaining);die;*/
														//echo ($daysRemaining/30)*$monthly_leave;
														
														$start_month=date('m', strtotime( $date->format('Y-m-d H:i:s')));
														$end_month =str_pad($leave['month'],2,'0',STR_PAD_LEFT);
														//echo $start_month."--".$end_month;
														if($start_month==$end_month && $remaining_first_leave_iteration == 0)
														{
															$remaining_first_leave_iteration =1;
															$remaining_leave = $remaining_leave+($daysRemaining/30)*$monthly_leave;
															//echo "First time";
														}
														else
														{
															//echo "iteration time";
															//$remaining_leave+=$monthly_leave+ ;
															$remaining_leave = $remaining_leave+$monthly_leave;
														}
														//echo $remaining_leave;
                                                        $present_days = isset($leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['present_days'])?$leave_reports_data[$leave['year'].str_pad($leave['month'],2,'0',STR_PAD_LEFT)]['present_days']:0;
                                                        if($unpaid_leave > 0){
                                                            $salary = $employee_data['salary'];
                                                            //$salary_per_day = $salary / $db->get_month_days(str_pad($leave['month'],2,'0',STR_PAD_LEFT),$leave['year']);
                                                            $salary_per_day = $salary / 30;
                                                            $salary = $salary - ($unpaid_leave * $salary_per_day);
                                                            $salary = round($salary,2);
                                                        }else{
                                                            $salary = $employee_data['salary'];
                                                        }
                                                        $remaining_leave = $remaining_leave - $paid_leave;
                                                        ?>
                                                        <tr>
                                                            <td><?=$leave['year'].'-'.str_pad($leave['month'],2,'0',STR_PAD_LEFT);?></td>
                                                            <td><?=$leave['taken_leave'];?></td>
                                                            <td><?=$paid_leave;?></td>
                                                            <td><?=$unpaid_leave;?></td>
                                                            <td><?=$leave_balance."=====".$remaining_leave;?></td>
                                                            <td><?=$present_days;?></td>
                                                            <td><?=$salary;?></td>
                                                        </tr>
                                            <?php
                                                    }
                                                }
                                            ?>
                                            </tbody>
                                        </table>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div>
    <strong>Copyright &copy; 2016-2017<a href="#"> &nbsp;OM</a>.</strong> All rights reserved.
</footer>
<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="plugins/morris/morris.min.js"></script> -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function () {

        $("#month_filter").datepicker(
            {
                format: "MM",
                startView: 1,
                minViewMode: 1,
                maxViewMode: 1,
                autoclose: true
            }).on('changeDate', function () {
            $('#hidden_month_filter').val($('#month_filter').datepicker('getFormattedDate', 'm'));
        });

        $("#year_filter").datepicker(
            {
                autoclose: true,
                format: "yyyy",
                startView: 2,
                minViewMode: 2,
                maxViewMode: 2
            });
        $('#leave-report-table').DataTable();
    });
</script>
</body>
</html>
