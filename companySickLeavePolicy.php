	<?php include_once 'menu.php';?>
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Company Leave Policy </h1>
			<ol class="breadcrumb">
				<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Company Leave Policy</li>
			</ol>
		</section>
		<section class="content">
                    	<div class="row">
                            <?php 
                             $leavePolicy = getCompanyPolicy();
                             if (isset($leavePolicy) && count($leavePolicy) > 0){
                                        $diff_week_off = $leavePolicy['diff_week_off'];
                                        $select_week_off = explode(",", $leavePolicy['select_week_off']);
                                        $allot_from = $leavePolicy['allot_from'];
                                        $avail_from = $leavePolicy['avail_from'];
                                        $allot_after_month = $leavePolicy['allot_after_month'];
                                        $allot_after_days = $leavePolicy['allot_after_days'];
                                        $avail_after_month = $leavePolicy['avail_after_month'];
                                        $avail_after_days = $leavePolicy['avail_after_days'];
                                        $total_leaves = $leavePolicy['total_leaves'];
                                        $auto_allotment = (isset($leavePolicy['auto_allotment']))?$leavePolicy['auto_allotment']:0;
                                        $allot = $leavePolicy['allot'];
                                        $allot_on = $leavePolicy['allot_on'];
                                        $allot_as_per = $leavePolicy['allot_as_per'];
                                        $min_days = $leavePolicy['min_days'];
                                        $carry_fwd = (isset($leavePolicy['carry_fwd']))?$leavePolicy['carry_fwd']:0;
                                        $monthly_carry_fwd = (isset($leavePolicy['monthly_carry_fwd']))?$leavePolicy['monthly_carry_fwd']:0;
                                        $monthly_carry_fwd_value = $leavePolicy['monthly_carry_fwd_value'];
                                        $laps_if_unavailed = $leavePolicy['laps_if_unavailed'];
                                        $yearly_carry_fwd = (isset($leavePolicy['yearly_carry_fwd']))?$leavePolicy['yearly_carry_fwd']:0;
                                        $yearly_carry_fwd_value = $leavePolicy['yearly_carry_fwd_value'];
                                        $leave_encashment = (isset($leavePolicy['leave_encashment']))?$leavePolicy['leave_encashment']:0;
                                        $leave_encashment_min_balance = $leavePolicy['leave_encashment_min_balance'];
                                        $leave_encashment_month = $leavePolicy['leave_encashment_month'];
                                        $id = $leavePolicy['id'];
                                        $action = "update";
                             }else{
                                        $diff_week_off = "";
                                        $select_week_off = array();
                                        $allot_from = "";
                                        $avail_from = "";
                                        $allot_after_month = "";
                                        $allot_after_days = "";
                                        $avail_after_month = "";
                                        $avail_after_days = "";
                                        $total_leaves = "";
                                        $auto_allotment = 0;
                                        $allot = "";
                                        $allot_on = "";
                                        $allot_as_per = "";
                                        $min_days = "";
                                        $carry_fwd = 0;
                                        $monthly_carry_fwd = 0;
                                        $monthly_carry_fwd_value = "";
                                        $laps_if_unavailed = "";
                                        $yearly_carry_fwd = 0;
                                        $yearly_carry_fwd_value = "";
                                        $leave_encashment = 0;
                                        $leave_encashment_min_balance = "";
                                        $leave_encashment_month = "";
                                        $action = "insert";
                             }
                             
                             
                            
            if(!empty($_POST) && isset($_POST['diffweekoff'])){
                    $diff_week_off = $_POST['diffweekoff'];
                    $select_week_off = $_POST['selectWeekOff'];
                    $select_week_off_str = implode(",", $_POST['selectWeekOff']);
                    $allot_from = $_POST['allot_from'];
                    $avail_from = $_POST['avail_from'];
                    $allot_after_month = $_POST['allot_after_month'];
                    $allot_after_days = $_POST['allot_after_days'];
                    $avail_after_month = $_POST['avail_after_month'];
                    $avail_after_days = $_POST['avail_after_days'];
                    $total_leaves = $_POST['total_leaves'];
                    $auto_allotment = (isset($_POST['auto_allotment']))?$_POST['auto_allotment']:0;
                    $allot = $_POST['allot'];
                    $allot_on = $_POST['allot_on'];
                    $allot_as_per = $_POST['allot_as_per'];
                    $min_days = $_POST['min_days'];
                    $carry_fwd = (isset($_POST['carryFwd']))?$_POST['carryFwd']:0;
                    $monthly_carry_fwd = (isset($_POST['monthCarryFwd']))?$_POST['monthCarryFwd']:0;
                    $monthly_carry_fwd_value = $_POST['monthCarryFwdTxt'];
                    $laps_if_unavailed = $_POST['laps_if_unavailed'];
                    $yearly_carry_fwd = (isset($_POST['yearCarryFwd']))?$_POST['yearCarryFwd']:0;
                    $yearly_carry_fwd_value = $_POST['yearCarryFwdTxt'];
                    $leave_encashment = (isset($_POST['leave_encashment']))?$_POST['leave_encashment']:0;
                    $leave_encashment_min_balance = $_POST['leave_encashment_min_balance'];
                    $leave_encashment_month = $_POST['leave_encashment_month'];
                    
                    if ($action == "insert"){
                    $sql = "insert into company_sick_leave_policy set 
                                diff_week_off = '$diff_week_off',
                                select_week_off = '$select_week_off_str',
                                allot_from = '$allot_from',
                                avail_from = '$avail_from',
                                allot_after_month = '$allot_after_month',
                                allot_after_days = '$allot_after_days',
                                avail_after_month = '$avail_after_month',
                                avail_after_days = '$avail_after_days',
                                total_leaves = '$total_leaves',
                                auto_allotment = '$auto_allotment',
                                allot = '$allot',
                                allot_on = '$allot_on',
                                allot_as_per = '$allot_as_per',
                                min_days = '$min_days',
                                carry_fwd = '$carry_fwd',
                                monthly_carry_fwd = '$monthly_carry_fwd',
                                monthly_carry_fwd_value = '$monthly_carry_fwd_value',
                                laps_if_unavailed = '$laps_if_unavailed',
                                yearly_carry_fwd = '$yearly_carry_fwd',
                                yearly_carry_fwd_value = '$yearly_carry_fwd_value',
                                leave_encashment = '$leave_encashment',
                                leave_encashment_min_balance = '$leave_encashment_min_balance',
                                leave_encashment_month = '$leave_encashment_month'";
                    }else{
                        $sql = "Update company_sick_leave_policy set 
                                diff_week_off = '$diff_week_off',
                                select_week_off = '$select_week_off_str',
                                allot_from = '$allot_from',
                                avail_from = '$avail_from',
                                allot_after_month = '$allot_after_month',
                                allot_after_days = '$allot_after_days',
                                avail_after_month = '$avail_after_month',
                                avail_after_days = '$avail_after_days',
                                total_leaves = '$total_leaves',
                                auto_allotment = '$auto_allotment',
                                allot = '$allot',
                                allot_on = '$allot_on',
                                allot_as_per = '$allot_as_per',
                                min_days = '$min_days',
                                carry_fwd = '$carry_fwd',
                                monthly_carry_fwd = '$monthly_carry_fwd',
                                monthly_carry_fwd_value = ".(int)$monthly_carry_fwd_value.",
                                laps_if_unavailed = '$laps_if_unavailed',
                                yearly_carry_fwd = '$yearly_carry_fwd',
                                yearly_carry_fwd_value = ".(int)$yearly_carry_fwd_value.",
                                leave_encashment = '$leave_encashment',
                                leave_encashment_min_balance = '$leave_encashment_min_balance',
                                leave_encashment_month = '$leave_encashment_month'
                                WHERE id = '$id'";
                    }
                                $db->save($sql);
                                clearEmpLeaveTable();
                                header('Location: ' . basename($_SERVER['PHP_SELF']));
                                exit;
                    
            }
            
        ?>
                            <form method="post" name="companySickLeaveForm">
                            <div class="col-md-12">
                                <div class="box box-primary">
                                    <div class="box-body">
                                       <div class="col-md-4"> 
                                            <div class="form-group">
                                                <label for="DiffWeekOff">Different week off : </label>
                                                <input type="radio" name="diffweekoff" value="2" <?php echo (isset($diff_week_off) && $diff_week_off == '2')?'checked="checked"':''; ?> /> 2 days
                                                <input type="radio" name="diffweekoff" value="1.5" <?php echo (isset($diff_week_off) && $diff_week_off == '1.5')?'checked="checked"':''; ?> /> 1.5 day
                                                <input type="radio" name="diffweekoff" value="1" <?php echo (isset($diff_week_off) && $diff_week_off == '1')?'checked="checked"':''; ?> /> 1 day
                                                <input type="radio" name="diffweekoff" value="0.5" <?php echo (isset($diff_week_off) && $diff_week_off == '0.5')?'checked="checked"':''; ?> /> 0.5 day
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="selectWeekOff">Select Week Off : </label>

                                                <select  name="selectWeekOff[]" id="selectWeekOff" class="form-control" tabindex="1" multiple="multiple">
                                                    <option value="mon" <?php echo (isset($select_week_off) && in_array("mon",$select_week_off))?"selected='selected'":""; ?>>Monday</option>
                                                        <option value="tue" <?php echo (isset($select_week_off) && in_array("tue",$select_week_off))?"selected='selected'":""; ?>>Tuesday</option>
                                                        <option value="wed" <?php echo (isset($select_week_off) && in_array("wed",$select_week_off))?"selected='selected'":""; ?>>Wednesday</option>
                                                        <option value="thu" <?php echo (isset($select_week_off) && in_array("thu",$select_week_off))?"selected='selected'":""; ?>>Thursday</option>
                                                        <option value="fri" <?php echo (isset($select_week_off) && in_array("fri",$select_week_off))?"selected='selected'":""; ?>>Friday</option>
                                                        <option value="sat" <?php echo (isset($select_week_off) && in_array("sat",$select_week_off))?"selected='selected'":""; ?>>Saturday</option>
                                                        <option value="sun" <?php echo (isset($select_week_off) && in_array("sun",$select_week_off))?"selected='selected'":""; ?>>Sunday</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                            </div>
				<div class="col-md-6">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Allotment :</h3>
						</div>
						<div class="box-body">
                                                    <div class="col-md-6 alott_wrapper">
							<div class="form-group">
                                                                <fieldset>
                                                                    <legend>Allot</legend>
                                                                    <div class="input-group date">
                                                                            <input type="radio" name="allot_from" id="datepicker" value="DOJ" <?php  echo ($allot_from == 'DOJ')?"checked='checked'":""; ?> > Date of Join
                                                                    </div>
                                                                    <div class="input-group date">
                                                                            <input type="radio" name="allot_from" id="datepicker" value="CD" <?php  echo ($allot_from == 'CD')?"checked='checked'":""; ?> > Confermation Date
                                                                    </div>
                                                                    <div class="input-group date">
                                                                            <input type="radio" name="allot_from" id="datepicker" value="PD" <?php  echo ($allot_from == 'PD')?"checked='checked'":""; ?> > Prob.Date
                                                                    </div>
                                                                </fieldset>
							</div>
							<div class="form-group">
								<label>Month :</label>
								<div class="input-group date">
									<input type="text" id="datepicker" name="allot_after_month" class="form-control pull-right" value="<?php echo $allot_after_month; ?>">
								</div>
							</div>
							<div class="form-group">
								<label>Days :</label>
								<div class="input-group date">
									<input type="text" id="datepicker" name="allot_after_days" class="form-control pull-right" value="<?php echo $allot_after_days; ?>">
								</div>
							</div>
                                                    </div>
                                                    <div class="col-md-6 avail_wrapper">
                                                        <div class="form-group">
                                                            <fieldset>
                                                                <legend>Avail From</legend>

                                                                        <div class="input-group date">
                                                                                <input type="radio" id="datepicker" name="avail_from" value="DOJ" <?php  echo ($avail_from == 'DOJ')?"checked='checked'":""; ?>> Date of Join
                                                                        </div>
                                                                        <div class="input-group date">
                                                                                <input type="radio" id="datepicker" name="avail_from" value="CD" <?php  echo ($avail_from == 'CD')?"checked='checked'":""; ?> > Confermation Date
                                                                        </div>
                                                                        <div class="input-group date">
                                                                                <input type="radio" id="datepicker" name="avail_from" value="PD" <?php  echo ($avail_from == 'PD')?"checked='checked'":""; ?> > Prob.Date
                                                                        </div>
                                                            </fieldset>
                                                        </div>
							<div class="form-group">
								<label>Month :</label>
								<div class="input-group date">
									<input type="text" id="datepicker" name="avail_after_month" value="<?php  echo $avail_after_month; ?>" class="form-control pull-right">
								</div>
							</div>
							<div class="form-group">
								<label>Days :</label>
								<div class="input-group date">
									<input type="text" id="datepicker" name="avail_after_days" value="<?php  echo $avail_after_days; ?>" class="form-control pull-right">
								</div>
							</div>
                                                    </div>
                                                    <div class="col-md-12 col-md-offset-5">
                                                        <div class="form-group totalLeave col-centered">
                                                                <label for="exampleInputEmail1">Total Leaves :</label>
                                                                <input type="text" value="<?php  echo $total_leaves; ?>" name="total_leaves" placeholder="Enter Total leaves" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-md-offset-5">
                                                        <div class="form-group">
                                                                    <input type="checkbox" placeholder="Enter Total leaves" name="auto_allotment" class="auto_allotment" value="1" <?php  echo ($auto_allotment == '1')?"checked='checked'":""; ?> >
                                                                <label for="exampleInputPassword1">Auto Allotment</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
									
									<div class="form-group">
										<label for="exampleInputPassword1">Allot : </label>
										<select  class="form-control" name="allot" <?php  echo ($auto_allotment != '1')?'disabled="disabled"':''; ?>>
											<option value="monthly" <?php echo ($allot == "montly")?"selected='selected'":""; ?>>Monthly</option>
                                                                                        <option value="quarterly" <?php echo ($allot == "quarterly")?"selected='selected'":""; ?>>quarterly</option>
                                                                                        <option value="yearly" <?php echo ($allot == "yearly")?"selected='selected'":""; ?>>Yearly</option>
                                                                                        <option value="half_yearly" <?php echo ($allot == "half_yearly")?"selected='selected'":""; ?>>Half Yearly</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="exampleInputPassword1">Allot On: </label>
										<select  class="form-control" name="allot_on" <?php  echo ($auto_allotment != '1')?'disabled="disabled"':''; ?>>
											<option value="pay_days" <?php echo ($allot_on == "pay_days")?"selected='selected'":""; ?>>Pay Days</option>
                                                                                        <option value="present_days" <?php echo ($allot_on == "present_days")?"selected='selected'":""; ?>>Present days</option>
                                                                                        <option value="none" <?php echo ($allot_on == "none")?"selected='selected'":""; ?>>None</option>
										</select>
									</div>
                                                                    </div>
                                                                    <div class="col-md-4">
									<div class="form-group">
										<label for="exampleInputPassword1">As Per : </label>
										<select  class="form-control" name="allot_as_per" <?php  echo ($auto_allotment != '1')?'disabled="disabled"':''; ?>>
											<option value="current_month" <?php echo ($allot_as_per == "current_month")?"selected='selected'":""; ?>>Current Month</option>
                                                                                        <option value="previous_month" <?php echo ($allot_as_per == "previous_month")?"selected='selected'":""; ?>>Previous Month</option>
                                                                                        <option value="none" <?php echo ($allot_as_per == "none")?"selected='selected'":""; ?>>None</option>
										</select>
									</div>
								</div>
                                                    <div class="col-md-4 col-md-offset-5">
                                                        <div class="form-group">
                                                                <label for="exampleInputPassword1">Min Days : </label>
                                                                <input type="text" placeholder="Enter Min Days" class="form-control" name="min_days" value="<?php echo $min_days; ?>">
                                                        </div>
                                                    </div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="box box-primary">
						<form role="form">
							<div class="box-body">
                                                            <div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="checkbox" placeholder="" name="carryFwd" value="1" <?php echo ($carry_fwd == '1')?"checked='checked'":""; ?> />
										<label for="exampleInputPassword1">Carry Fwd</label>
									</div>
                                                                        <div class="form-group">
										<input type="checkbox" placeholder="" name="monthCarryFwd" <?php echo ($carry_fwd != '1')?'disabled="disabled"':''; ?> value="1" <?php echo ($monthly_carry_fwd == '1')?"checked='checked'":""; ?> >
										<label for="exampleInputPassword1">Monthly Carry Fwd</label>
										<input type="text" name="monthCarryFwdTxt" placeholder="Enter Monthly Carry Fwd" class="form-control" style="width: 190px;" <?php echo ($carry_fwd != '1')?'disabled="disabled"':''; ?> value="<?php echo $monthly_carry_fwd_value; ?>">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="exampleInputPassword1">Laps if Unavailed : </label>
										<select  class="form-control lapsifselect" name="laps_if_unavailed" <?php echo ($carry_fwd != '1')?'disabled="disabled"':''; ?>>
											<option value="JAN" <?php echo ($laps_if_unavailed == "JAN")?"selected='selected'":""; ?>>JAN</option>
                                                                                        <option value="FEB" <?php echo ($laps_if_unavailed == "FEB")?"selected='selected'":""; ?>>FEB</option>
                                                                                        <option value="MAR" <?php echo ($laps_if_unavailed == "MAR")?"selected='selected'":""; ?>>MAR</option>
                                                                                        <option value="APR" <?php echo ($laps_if_unavailed == "APR")?"selected='selected'":""; ?>>APR</option>
                                                                                        <option value="MAY" <?php echo ($laps_if_unavailed == "MAY")?"selected='selected'":""; ?>>MAY</option>
                                                                                        <option value="JUN" <?php echo ($laps_if_unavailed == "JUN")?"selected='selected'":""; ?>>JUN</option>
                                                                                        <option value="JULY" <?php echo ($laps_if_unavailed == "JULY")?"selected='selected'":""; ?>>JULY</option>
                                                                                        <option value="AUG" <?php echo ($laps_if_unavailed == "AUG")?"selected='selected'":""; ?>>AUG</option>
                                                                                        <option value="SEPT" <?php echo ($laps_if_unavailed == "SEPT")?"selected='selected'":""; ?>>SEPT</option>
                                                                                        <option value="OCT" <?php echo ($laps_if_unavailed == "OCT")?"selected='selected'":""; ?>>OCT</option>
                                                                                        <option value="NOV" <?php echo ($laps_if_unavailed == "NOV")?"selected='selected'":""; ?>>NOV</option>
                                                                                        <option value="DEC" <?php echo ($laps_if_unavailed == "DEC")?"selected='selected'":""; ?>>DEC</option>
										</select>
									</div>
								</div>
								
								<div class="col-md-12">
									<div class="form-group">
										<input type="checkbox" placeholder="" value="1" <?php echo ($yearly_carry_fwd == '1')?"checked='checked'":""; ?> name="yearCarryFwd" <?php echo ($carry_fwd != '1')?'disabled="disabled"':''; ?> >
										<label for="exampleInputPassword1">Yearly Carry Fwd</label>
										<input type="text" name="yearCarryFwdTxt" placeholder="Enter Yearly Carry Fwd" class="form-control" style="width: 190px;" <?php echo ($carry_fwd != '1')?'disabled="disabled"':''; ?> value="<?php echo $yearly_carry_fwd_value; ?>">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input type="checkbox" value="1" <?php echo ($leave_encashment == '1')?"checked='checked'":""; ?> placeholder="Enter Total leaves" name="leave_encashment" >
										<label for="exampleInputPassword1">Leave Encashment</label>
									</div>
								</div>
                                                                </div>
                                                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputPassword1">Minimum Balance : </label>
										<input type="text" name="leave_encashment_min_balance" value="<?php echo $leave_encashment_min_balance; ?>" placeholder="Enter Min Days" class="form-control" style="width: 190px;">
									</div>
								</div>
                                                            <div class="col-md-4">
                                                                    <div class="form-group">
                                                                                <label for="exampleInputPassword1">&nbsp;</label>
										<select  name="leave_encashment_month" class="form-control">
											<option value="JAN" <?php echo ($leave_encashment_month == "JAN")?"selected='selected'":""; ?>>JAN</option>
                                                                                        <option value="FEB" <?php echo ($leave_encashment_month == "FEB")?"selected='selected'":""; ?>>FEB</option>
                                                                                        <option value="MAR" <?php echo ($leave_encashment_month == "MAR")?"selected='selected'":""; ?>>MAR</option>
                                                                                        <option value="APR" <?php echo ($leave_encashment_month == "APR")?"selected='selected'":""; ?>>APR</option>
                                                                                        <option value="MAY" <?php echo ($leave_encashment_month == "MAY")?"selected='selected'":""; ?>>MAY</option>
                                                                                        <option value="JUN" <?php echo ($leave_encashment_month == "JUN")?"selected='selected'":""; ?>>JUN</option>
                                                                                        <option value="JULY" <?php echo ($leave_encashment_month == "JULY")?"selected='selected'":""; ?>>JULY</option>
                                                                                        <option value="AUG" <?php echo ($leave_encashment_month == "AUG")?"selected='selected'":""; ?>>AUG</option>
                                                                                        <option value="SEPT" <?php echo ($leave_encashment_month == "SEPT")?"selected='selected'":""; ?>>SEPT</option>
                                                                                        <option value="OCT" <?php echo ($leave_encashment_month == "OCT")?"selected='selected'":""; ?>>OCT</option>
                                                                                        <option value="NOV" <?php echo ($leave_encashment_month == "NOV")?"selected='selected'":""; ?>>NOV</option>
                                                                                        <option value="DEC" <?php echo ($leave_encashment_month == "DEC")?"selected='selected'":""; ?>>DEC</option>
										</select>
									</div>
                                                                </div>
                                                        </div>
 							</div>
							<div class="box-footer">
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
						</form>
					</div>
				</div>
                            </form>
			</div>
		</section>
	</div>
	<footer class="main-footer">
		<div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div>
		<strong>Copyright &copy; 2016-2017<a href="#"> &nbsp;OM</a>.</strong> All rights reserved.
	</footer>
	<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="plugins/morris/morris.min.js"></script> -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$.widget.bridge('uibutton', $.ui.button);
	$(function () {
		$("#example1").DataTable();
		$('#reservation').daterangepicker({
			"locale": {
				"format": "DD/MM/YYYY"
			}
		});
	});
        
$(document).ready(function () {
    var selectLimit = 0;
    var dayText = "Day";
    $("input[name='diffweekoff']").on("change", function(){ 
        if (this.value == "2"){
            selectLimit = 2;
            dayText = "Days";
        }else if (this.value == "1.5"){
            selectLimit = 2;
            dayText = "Days";
        }else if (this.value == "1"){
            selectLimit = 1;
            dayText = "Day";
        }else if (this.value == "0.5")
        {
            selectLimit = 1;
            dayText = "Day";
        }
        $('#selectWeekOff').val([]);
    });
    var last_valid_selection = null;
    
      $('#selectWeekOff').change(function(event) {
        if (selectLimit == 0){
            alert('Please select Different Week Off first');
        }else if ($(this).val().length > selectLimit) {
            
          alert('You can select only '+ selectLimit +' '+dayText+'!');
          $(this).val(last_valid_selection);
        }else {
          last_valid_selection = $(this).val();
        }
      });
      
      $("input[name='carryFwd']").on("change", function(){
          if($('input[name="carryFwd"]').prop('checked')) {
              
            $("input[name='monthCarryFwd']").removeAttr("disabled");
            $("input[name='monthCarryFwd']").prop("checked", true);
            
            $("input[name='monthCarryFwdTxt']").removeAttr("disabled");
            $("input[name='monthCarryFwdTxt']").val("");
            
            $("input[name='yearCarryFwd']").removeAttr("disabled");
            $("input[name='yearCarryFwd']").prop("checked", true);
            
            $("input[name='yearCarryFwdTxt']").removeAttr("disabled");
            $("input[name='yearCarryFwdTxt']").val("");
            
            $("select[name='laps_if_unavailed']").removeAttr("disabled");
            
        } else {
            $("input[name='monthCarryFwd']").prop("checked", false);
            $("input[name='monthCarryFwd']").attr("disabled","disabled");
            
            $("input[name='monthCarryFwdTxt']").val("");
            $("input[name='monthCarryFwdTxt']").attr("disabled","disabled");
            
            $("input[name='yearCarryFwd']").prop("checked", false);
            $("input[name='yearCarryFwd']").attr("disabled","disabled");
            
            $("input[name='yearCarryFwdTxt']").val("");
            $("input[name='yearCarryFwdTxt']").attr("disabled","disabled");
            
            $("select[name='laps_if_unavailed']").attr("disabled","disabled");
        }
      
      });
      
        $("input[name='auto_allotment']").on("change", function(){
          if($('input[name="auto_allotment"]').prop('checked')) {
              $("select[name='allot']").removeAttr("disabled");
              $("select[name='allot_on']").removeAttr("disabled");
              $("select[name='allot_as_per']").removeAttr("disabled");
          }else{
              $("select[name='allot']").attr("disabled","disabled");
              $("select[name='allot_on']").attr("disabled","disabled");
              $("select[name='allot_as_per']").attr("disabled","disabled");
          }
        });
});

function chkWeekOffChanged(obj) {
      limitSelectBox(obj.value);
}

function limitSelectBox(limit)
{
    
}
</script>
</body>
</html>
	