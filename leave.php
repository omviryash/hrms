	<?php include_once 'menu.php';?>
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Employee Leave Details </h1>
			<ol class="breadcrumb">
				<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Employee Leave Details</li>
			</ol>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Employee Leave Information</h3>
						</div>
						<form role="form" method="post" action="leave.php">
							<div class="box-body">
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputEmail1">Employee Name:</label>
										<select  class="form-control" name="empid">
										<?php 
											$sql = "Select * from employee";
											$data = $db->fetch($sql);
											for($i=0;$i<count($data);$i++){
												echo "<option value=".$data[$i]['id'].">".$data[$i]['empname']."</option>";
											}
										?>
										</select>
									</div>
									<!-- <div class="form-group">
										<label for="exampleInputPassword1">Employee Code : </label>
										<span class="form-control">CLIF-001</span>
									</div> -->
									<div class="form-group">
										<label for="exampleInputEmail1">Leave Type</label>
										<select  class="form-control" id="leavetype" name="leavetype">
											<option value="P">P</option>
											<option value="LOP">LOP</option>
											<option value="SL">SL</option>
											<option value="CL">CL</option>
											<option value="PL">PL</option>
											<option value="CO">CO</option>
											<option value="CW">CW</option>
											<option value="12P">1/2P</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputPassword1">Leave Date</label>
										<input type="text" placeholder="Select Confirmation Date" id="leavedate" name="leavedate" class="form-control">
									</div>
								</div>
 							</div>
							<div class="box-footer">
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<?php
				if(!empty($_POST)){
	//				echo "<pre>";
	//				print_r($_POST);
					$empid = $_POST['empid'];
					$leaveType = $_POST['leavetype'];
	//				print_r(explode(' - ',$_POST['leavedate']));
					$leavedate = explode(' - ',$_POST['leavedate']);
					$starDate = $leavedate[0];
					$endDate  = $leavedate[1];
                                        $companyPolicySql = "select * from company_sick_leave_policy";
                                        $companyPolicyData = $db->fetch($companyPolicySql);
                                        if (count($companyPolicyData) > 0){
                                            $WH_day_str = " ".$companyPolicyData[0]['select_week_off'];
                                            
					if($starDate==$endDate){
						$sql = "insert into emp_leave(leave_type,leave_date,employee_id) value('".$leaveType."','".$db->yyyymmdd($starDate)."',".$empid.")";
						$db->save($sql);
					}else{
						$numberDays = getdate(abs(strtotime($db->yyyymmdd($starDate)) - strtotime($db->yyyymmdd($endDate))))['mday'];
						for($i=0;$i<$numberDays;$i++){
							$leaveDay = date('d/m/Y', strtotime($db->yyyymmdd($starDate).'+'.($i).'day'));
							$dayName  = strtolower(date('D', strtotime($db->yyyymmdd($leaveDay))));
							//echo "leaveDay : ".$leaveDay ." dayName : ".$dayName ."<br> ".$WH_day_str ."<br />";
                                                        $WHCheck = strpos($WH_day_str,  $dayName);
							if($WHCheck === false){
								$sql = "insert into emp_leave(leave_type,leave_date,employee_id) value('".$leaveType."','".$db->yyyymmdd($leaveDay)."',".$empid.")";
								$db->save($sql);
							}else{
								$sql = "insert into emp_leave(leave_type,leave_date,employee_id) value('WH','".$db->yyyymmdd($leaveDay)."',".$empid.")";
								$db->save($sql);
							}
						}
					}
				}
                                	header('Location: ' . basename($_SERVER['PHP_SELF']));
                                }
			?>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">List of Employee Leave : </h3>
				</div>
				<div class="box-body">
					<table id="example1AA" class="table table-bordered table-striped" style="overflow: scroll;">
						<thead>
							<tr>
								<th>Month</th>
								<th>01</th>
								<th>02</th>
								<th>03</th>
								<th>04</th>
								<th>05</th>
								<th>06</th>
								<th>07</th>
								<th>08</th>
								<th>09</th>
								<th>10</th>
								<th>11</th>
								<th>12</th>
								<th>13</th>
								<th>14</th>
								<th>15</th>
								<th>16</th>
								<th>17</th>
								<th>18</th>
								<th>19</th>
								<th>20</th>
								<th>21</th>
								<th>22</th>
								<th>23</th>
								<th>24</th>
								<th>25</th>
								<th>26</th>
								<th>27</th>
								<th>28</th>
								<th>29</th>
								<th>30</th>
								<th>31</th>
							</tr>
						</thead>
						<tbody>
<?php 
//ini_set('memory_limit', '-1');
for($i=1;$i<13;$i++){
	$sql = "SELECT a.*,DATE_FORMAT(a.leave_date,'%d') as days FROM emp_leave a where DATE_FORMAT(a.leave_date, '%m') = ".$i." order by leave_date";
	$month = $db ->fetch($sql);
	if(!empty($month) && count($month)>0){
		$str = "<tr>";
		$str = $str."<td>". date("M",  mktime(0, 0, 0, $i, 10))."</td>";
		$cnt = 1;
		$days = array_column($month,'days');
//		print_r($days);
		$cnt = 0;
		for($j=1;$j<32;$j++){
			if(in_array($j,$days)){
				$str= $str."<td>".$month[$cnt++]['leave_type']."</td>";
			}else{
				$str= $str."<td></td>";
			}
		}
		$str = $str. "</tr>";
		echo $str;
	}
}
?>
  						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
	<footer class="main-footer">
		<div class="pull-right hidden-xs"><b>Version</b> 1.0.0</div>
		<strong>Copyright &copy; 2016-2017<a href="#"> &nbsp;OM</a>.</strong> All rights reserved.
	</footer>
	<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="plugins/morris/morris.min.js"></script> -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/pages/dashboard.js"></script>
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
$.widget.bridge('uibutton', $.ui.button);
	$(function () {
		$("#example1").DataTable();
		$('#leavedate').daterangepicker({
			"locale": {
				"format": "DD/MM/YYYY"
			}
		});
	});
</script>
</body>
</html>
