<?php
/**
 * This file is part of the array_column library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Ben Ramsey (http://benramsey.com)
 * @license http://opensource.org/licenses/MIT MIT
 */
if (!function_exists('array_column')) {
    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();
        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }
        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }
        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }
        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }
        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;
        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }
        $resultArray = array();
        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;
            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }
            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }
            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }
        }
        return $resultArray;
    }
}

function clearEmpLeaveTable(){
    global $db;
    
    $sql = "TRUNCATE TABLE emp_leave";
    $db->query($sql);
        
}

function getCompanyPolicy(){
    global $db;
    
    $company_policy_sql = "SELECT * FROM company_sick_leave_policy";
    $company_policy_data = $db->getOneRow($company_policy_sql);

    return $company_policy_data;
}

function getEmpLeaveByDateRange($empId, $month, $year){
    global $db;
    
    $leave_type_sql = "SELECT * FROM emp_leave where employee_id = ".$empId ." and MONTH(leave_date) = '".$month."' AND YEAR(leave_date) = '".$year."'";
    $leave_type_data = $db->fetch($leave_type_sql);

    return $leave_type_data;
}

function getEmpById($empId = 0){
    global $db;
    if($empId != 0){
        $employee_sql = "SELECT * FROM employee WHERE id = '$empId'";
    }else{
        $employee_sql = "SELECT * FROM employee";
    }
    $employee_data = $db->getOneRow($employee_sql);

    return $employee_data;
}

function displayAllotmentByTotalLeaves($empData,$company_policy_data)
{
    $totalLeave = $company_policy_data['total_leaves'];
    $allot = $company_policy_data['allot'];
    $allot_on = $company_policy_data['allot_on'];
    $allot_as_per = $company_policy_data['allot_as_per'];
    $allotFrom = $company_policy_data['allot_from'];
    $allotAfterDays = $company_policy_data['allot_after_days'];
    $allotment = 0;
    $now = time(); // or your date as well
    if($allotFrom == 'DOJ'){
        $doj_date = strtotime($empData['dateofjoinin']);
        $datediff = $now - $doj_date;
        $totalDays = floor($datediff / (60 * 60 * 24)) + $allotAfterDays;
        if ($totalDays > 0){
            if (strtolower($allot) == 'monthly'){
                $allotment = (int)$totalLeave / 12;
            }else if (strtolower($allot) == 'quarterly'){
                $allotment = (int)$totalLeave / 4;
            } else if (strtolower($allot) == 'half_yearly')
            {
                $allotment = (int)$totalLeave / 2;
            }else if (strtolower($allot) == 'yearly')
            {
                $allotment = (int)$totalLeave;
            }
        }else{
            $allotment = 0;
        }
    }else if($allotFrom == 'CD'){
        $cd_date = strtotime($empData['confdate']);
        $datediff = $now - $cd_date;
        $totalDays = floor($datediff / (60 * 60 * 24)) + $allotAfterDays;
        if ($totalDays > 0){
            if ($allot == 'monthly'){
                $allotment = (int)$totalLeave / 12;
            }else if ($allot == 'quarterly'){
                $allotment = (int)$totalLeave / 4;
            } else if ($allot == 'half_yearly')
            {
                $allotment = (int)$totalLeave / 2;
            }else if ($allot == 'yearly')
            {
                $allotment = (int)$totalLeave;
            }
        }else{
            $allotment = 0;
        }
    }else if($allotFrom == 'PD'){
        $pd_date = strtotime($empData['prodate']);
        $datediff = $now - $pd_date;
        $totalDays = floor($datediff / (60 * 60 * 24)) + $allotAfterDays;
        if ($totalDays > 0){
            if ($allot == 'monthly'){
                $allotment = (int)$totalLeave / 12;
            }else if ($allot == 'quarterly'){
                $allotment = (int)$totalLeave / 4;
            } else if ($allot == 'half_yearly')
            {
                $allotment = (int)$totalLeave / 2;
            }else if ($allot == 'yearly')
            {
                $allotment = (int)$totalLeave;
            }
        }else{
            $allotment = 0;
        }
    }
    
    
    
    return $allotment;
}

function getAllotmentDate($employee_data, $company_policy_data)
{
    $allotFrom = $company_policy_data['allot_from'];
    $allotAfterMonth = $company_policy_data['allot_after_month'];
    $allotAfterDays = $company_policy_data['allot_after_days'];
    
    // Get Allotment Date
    if($allotFrom == 'DOJ'){
        $allotmentDate = $employee_data['dateofjoinin'];
    }else if($allotFrom == 'CD'){
        $allotmentDate = $employee_data['confdate'];
    }else if($allotFrom == 'PD'){
        $allotmentDate = $employee_data['prodate'];
    }
    $allotmentDateString = strtotime($allotmentDate);
    if ($allotAfterMonth && $allotAfterDays){
        $allotmentDateString = strtotime(date("Y-m-d",  strtotime("+".$allotAfterMonth. " month",$allotmentDateString)));
        $allotmentDateString = strtotime(date("Y-m-d",  strtotime("+".$allotAfterDays. " day",$allotmentDateString)));
    }else if ($allotAfterMonth && !$allotAfterDays){
        $allotmentDateString = strtotime(date("Y-m-d",  strtotime("+".$allotAfterMonth. " month",$allotmentDateString)));
    }else if (!$allotAfterMonth && $allotAfterDays){
        $allotmentDateString = strtotime(date("Y-m-d",  strtotime("+".$allotAfterDays. " day",$allotmentDateString)));
    }
    $empAllotmentDate = date("Y-m-d",$allotmentDateString);
    return $empAllotmentDate;
}

function getAvailDate($employee_data,$company_policy_data)
{
    $availFrom = $company_policy_data['avail_from'];
    $availAfterMonth = $company_policy_data['avail_after_month'];
    $availAfterDays = $company_policy_data['avail_after_days'];
    
    // Get Avail Date
    if($availFrom == 'DOJ'){
        $availDate = $employee_data['dateofjoinin'];
    }else if($availFrom == 'CD'){
        $availDate = $employee_data['confdate'];
    }else if($availFrom == 'PD'){
        $availDate = $employee_data['prodate'];
    }
    
    $availDateString = strtotime($availDate);
    if ($availAfterMonth && $availAfterDays){
        $availDateString = strtotime(date("Y-m-d",  strtotime("+".$availAfterMonth. " month",$availDateString)));
        $availDateString = strtotime(date("Y-m-d",  strtotime("+".$availAfterDays. " day",$availDateString)));
    }else if ($availAfterMonth && !$availAfterDays){
        $availDateString = strtotime(date("Y-m-d",  strtotime("+".$availAfterMonth. " month",$availDateString)));
    }else if (!$availAfterMonth && $availAfterDays){
        $availDateString = strtotime(date("Y-m-d",  strtotime("+".$availAfterDays. " day",$availDateString)));
    }
    $empAvailDate = date("Y-m-d",$availDateString);
    
    return $empAvailDate;
}

function getAllotmentSL($empData,$company_policy_data, $daysCount, $payDays = 0, $presentDays = 0){
    // get Allotment Date
    $getAllotmentDate = getAllotmentDate($empData,$company_policy_data);
    /*print_r($company_policy_data);die;*/
    $allotment = displayAllotmentByTotalLeaves($empData, $company_policy_data);
    $allot_on = $company_policy_data['allot_on'];
    $allot_as_per = $company_policy_data['allot_as_per'];
    $SL = 0;
    if (strtolower($allot_as_per) == 'current_month'){
        if ($allot_on == "pay_days"){
            $SL = ($payDays * $allotment)/$daysCount;
        }
        else if ($allot_on == "present_days"){
            $SL = ($presentDays * $allotment)/$daysCount;
        }
    }elseif(strtolower($allot_as_per) == 'previous month'){
        if ($allot_on == "pay_days"){
            $SL = ($payDays * $allotment)/$daysCount;
        }
        else if ($allot_on == "present_days"){
            $SL = ($presentDays * $allotment)/$daysCount;
        }
    }
    
    return round($SL,2);
}

function getAllotmentCO(){
    
}

function getAvailedSL(){
    
}

function getAvailedCO(){
    
}

function getClosingSL(){
    
}

function getClosingCO()
{
    
}

function getOpeningSL(){
    
}

function getOpeningCO(){
    
}
?>