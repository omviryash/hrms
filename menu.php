<?php include_once 'header.php';?>
<div class="wrapper">
	<header class="main-header">
		<a href="index.html" class="logo">
			<span class="logo-mini"><b>HRMS</b></span>
			<span class="logo-lg"><b>HRMS</b></span>
		</a>
		<nav class="navbar navbar-static-top">
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
							<span class="hidden-xs">Username</span>
						</a>
						<ul class="dropdown-menu">
							<li class="user-header">
								<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
								<p>Username - Senior HR</p>
							</li>
							<li class="user-footer">
								<div class="pull-left">
									<a href="profile.php" class="btn btn-default btn-flat">Profile</a>
								</div>
								<div class="pull-right">
									<a href="signout.php" class="btn btn-default btn-flat">Sign out</a>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="main-sidebar">
		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header">NAVIGATION</li>
				<li class="active treeview">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Menu</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li <?=in_array(basename($_SERVER["SCRIPT_FILENAME"]),array('index.php'))?'class="active"':'class=""'?>><a href="index.php"><i class="fa fa-circle-o"></i>Employee Details</a></li>
						<!--<li <?/*=in_array(basename($_SERVER["SCRIPT_FILENAME"]),array('leave.php'))?'class="active"':'class=""'*/?>><a href="leave.php"><i class="fa fa-circle-o"></i>Employee Leave</a></li>-->
						<li <?=in_array(basename($_SERVER["SCRIPT_FILENAME"]),array('companySickLeavePolicy.php'))?'class="active"':'class=""'?>><a href="companySickLeavePolicy.php"><i class="fa fa-circle-o"></i>Company Sick Leave Policy</a></li>
						<li <?=in_array(basename($_SERVER["SCRIPT_FILENAME"]),array('calender.php'))?'class="active"':'class=""'?>><a href="calender.php"><i class="fa fa-circle-o"></i>Company Calender</a></li>
						<!--<li <?/*=in_array(basename($_SERVER["SCRIPT_FILENAME"]),array('report.php'))?'class="active"':'class=""'*/?>><a href="report.php"><i class="fa fa-circle-o"></i>Report</a></li>-->
						<li <?=in_array(basename($_SERVER["SCRIPT_FILENAME"]),array('add_leave.php'))?'class="active"':'class=""'?>><a href="add_leave.php"><i class="fa fa-circle-o"></i>Add Employee Leave</a></li>
						<li <?=in_array(basename($_SERVER["SCRIPT_FILENAME"]),array('leave_report.php'))?'class="active"':'class=""'?>><a href="leave_report.php"><i class="fa fa-circle-o"></i>Leave Report</a></li>
					</ul>
				</li>
			</ul>
		</section>
	</aside>